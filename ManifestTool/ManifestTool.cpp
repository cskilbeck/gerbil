//////////////////////////////////////////////////////////////////////

#include "../common/common.h"
#include "../common/md5.h"
#include "../common/sha256.h"
#include "Arguments.h"
#include "WinResource.h"
#include "../Gerbil/gerbil_resource.h"
#include "ManifestTool_resource.h"

#include <list>
#include <string>
#include <vector>

#pragma comment(lib, "shlwapi.lib")

using std::list;
using std::string;
using std::vector;
using std::wstring;

//////////////////////////////////////////////////////////////////////

FILE *manifest_file = null;
wstr files_folder;
int files_scanned = 0;
uint64 total_files_size;

LOG_Context("ManifestTool");

//////////////////////////////////////////////////////////////////////

Arguments args;

enum ReturnCode
{
    SUCCESS = 0,
    ERROR_USAGE = 1,
    // ERROR_MAX_DOWNLOADS_OUT_OF_RANGE = 2,
    ERROR_UPDATING_LAUNCHER_RESOURCES = 3,
    ERROR_ICON_NOT_COMPATIBLE = 4,
    ERROR_LOADING_ICON_FILE = 5,
    ERROR_SAVING_LAUNCHER_EXE = 6,
    ERROR_LOADING_EXE_RESOURCE = 7,
    ERROR_SCANNING_FOLDER = 8,
    // ERROR_NO_FILESPECS = 9,
    ERROR_BAD_LIBRARIES_ARGUMENT = 10,
    ERROR_WRITING_TO_MANIFEST_FILE = 11
};

enum ScanFolderError
{
    OK = 0,
    ERROR_PERFORMING_CHECKSUM = 1,
    ERROR_CREATING_INTERIM_FOLDER = 2,
    ERROR_COPYING_TO_INTERIM_FOLDER = 3,
    ERROR_FINDING_NEXT_FILE = 4,
};

//////////////////////////////////////////////////////////////////////

bool EnsureFolderExists(wchar const *path)
{
    if(!FolderExists(path)) {
        LOG_Verbose("Creating folder %s", path);
        if(!SUCCEEDED(CreateFolder(path))) {
            DWORD e = GetLastError();
            LOG_Error("Error creating %s", path);
            SetLastError(e);
            return false;
        }
    }
    return true;
}

//////////////////////////////////////////////////////////////////////

int OpenManifestFile(wchar const *filename)
{
    if(!EnsureFolderExists(GetPathFromFilename(filename))) {
        ERROR_WRITING_TO_MANIFEST_FILE;
    }
    LOG_Info("Creating manifest file %s", filename);
    if(manifest_file == null && _wfopen_s(&manifest_file, filename, L"w") != 0) {
        return ERROR_WRITING_TO_MANIFEST_FILE;
    }
    return S_OK;
}

//////////////////////////////////////////////////////////////////////

void WriteManifest(wchar const *fmt, ...)
{
    if(manifest_file != null) {
        va_list v;
        va_start(v, fmt);
        wstr w = wstr::format(fmt, v);
        str s = WStrToNarrow(w);
        fprintf(manifest_file, "%s", s.c_str());
    }
}

//////////////////////////////////////////////////////////////////////

void WriteLNManifest(wchar const *fmt, ...)
{
    if(manifest_file != null) {
        va_list v;
        va_start(v, fmt);
        wstr w = wstr::format(fmt, v);
        str s = WStrToNarrow(w);
        fprintf(manifest_file, "%s\n", s.c_str());
    }
}

//////////////////////////////////////////////////////////////////////

void CloseManifestFile()
{
    if(manifest_file != null) {
        LOG_Verbose("Closing manifest file");
        fclose(manifest_file);
        manifest_file = null;
    }
}

//////////////////////////////////////////////////////////////////////

wstr HexString(byte const *s, int length_in_bytes)
{
    wstr t(length_in_bytes * 2 + 1);
    for(int i = 0; i < length_in_bytes; ++i) {
        wsprintf(t.buffer() + i * 2, L"%02x", s[i]);
    }
    return t;
}

//////////////////////////////////////////////////////////////////////

bool IsSpecial(wchar const *path)
{
    return path[0] == '.' && path[1] == 0 || path[0] == '.' && path[1] == '.' && path[2] == 0;
}

//////////////////////////////////////////////////////////////////////

struct File
{
    wstring path;
    wstring relative;

    File(wstring const &filepath, wstring const &relativepath) : path(filepath), relative(relativepath)
    {
    }

    int Output()
    {
        // the smallest factory in history
        Hash::Checksum *checksum = null;
        switch(args.checksum_type) {
        case Arguments::checksum_type_md5:
            checksum = new Hash::MD5();
            break;
        case Arguments::checksum_type_sha256:
            checksum = new Hash::SHA256();
            break;
        }

        uint64_t file_size;
        if(!ChecksumFile(path.c_str(), file_size, checksum)) {
            ReportWindowsError(L"Can't checksum %s\n", path.c_str());
            return ERROR_PERFORMING_CHECKSUM;
        }

        // download_path is where to download the file from relative to url_base
        wstring download_path = wstring(args.version) + wstring(L"/") + relative;

        wstr version_folder = PathJoin(args.interim_folder, args.version);
        wstr final_path = PathJoin(version_folder, relative.c_str());

        EnsureFolderExists(GetPathFromFilename(final_path.c_str()));

        if(!CopyFile(path.c_str(), final_path, false)) {
            ReportWindowsError(L"Can't copy %s to %s\n", path.c_str(), args.interim_folder.c_str());
            return 0;
        }

        WriteLNManifest(L"{\n\t\t\t\"file\":\"%s\",", DoubleSlash(wstr(relative.c_str()).c_str()));
        WriteLNManifest(L"\t\t\t\"size\":%lld,", file_size);
        WriteManifest(L"\t\t\t\"%s\":\"%s\"\n\t\t}", checksum->Name(), HexString(checksum->Result(), checksum->Size()).c_str());
        return SUCCESS;
    }
};

//////////////////////////////////////////////////////////////////////

vector<File> files;         // all the files it found
vector<wstring> folders;    // all the folders it found

//////////////////////////////////////////////////////////////////////
// ScanForFiles looks for files matching spec in a single folder

int ScanForFiles(wstr const &root_path, wstr const &current_dir, wchar const *spec)
{
    LOG_Context("ScanForFiles");

    WIN32_FIND_DATA find_data;
    wstr path = $(L"%s\\%s", current_dir.c_str(), spec);    // PathJoin with a wildcard as filename? best not...

    wstr relative_scan_dir;
    RelativePath(root_path, current_dir, relative_scan_dir);
    wstr relative_scan_filespec = $(L"%s\\%s", relative_scan_dir.c_str(), spec);

    int file_count = 0;
    LOG_Verbose("Checking %s", relative_scan_filespec.c_str());
    HANDLE dir = FindFirstFile(path, &find_data);
    if(dir == INVALID_HANDLE_VALUE) {
        if(GetLastError() != ERROR_FILE_NOT_FOUND) {
            ReportWindowsError(L"Can't FindFirstFile on %s\n", path.c_str());
        }
        return 0;
    }
    scoped[&]
    {
        LOG_Verbose("Found %d files in %s", file_count, relative_scan_filespec.c_str());
        FindClose(dir);
    };

    do {
        if((find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0) {

            wstr canon = MakePathCanonical(find_data.cFileName);
            wstr filename = PathJoin(MakePathCanonical(current_dir), canon);
            wstr relative;
            if(!RelativePath(root_path, filename, relative)) {
                LOG_Error("Can't make a relative path from %s to %s", root_path.c_str(), filename.c_str());
                return 0;
            }
            LOG_Verbose("Found %s", relative.c_str());
            uint64 file_size = FileSize(filename);
            if(file_size > 0 || !args.exclude_empty_files) {
                files.push_back(File(filename.c_str(), relative.c_str()));
                files_scanned += 1;
                total_files_size += FileSize(filename);
                file_count += 1;
            } else {
                LOG_Info("Skipping empty file %s", relative.c_str());
            }
        }
    } while(FindNextFile(dir, &find_data) != 0);

    return file_count;
}

//////////////////////////////////////////////////////////////////////
// ScanFolder
// find all the files in a folder and output names & checkums

// have to do this in 2 passes
// 1st pass, we scan for *.* (so we get all the folders)
// when we get a folder, we scan it with the filespecs for files only (non-recursively) then recurse into the folder

int ScanFolder(wstr const &root_path, wstr const &current_dir, vector<wstring> const &specs)
{
    LOG_Context("ScanFolder");

    // don't scan in . or ..
    if(IsSpecial(GetFilename(current_dir))) {
        return 0;
    }

    wstr path = $(L"%s\\*.*", current_dir.c_str());
    wstr relative_scan_dir;
    RelativePath(root_path, current_dir, relative_scan_dir);

    LOG_Debug("Scanning %s", relative_scan_dir.c_str());

    int entries_in_this_folder = 0;

    for(auto const &spec : specs) {
        entries_in_this_folder += ScanForFiles(root_path, current_dir, spec.c_str());
    }

    WIN32_FIND_DATA find_data;

    HANDLE dir = FindFirstFileEx(path, FindExInfoStandard, &find_data, FindExSearchLimitToDirectories, null, 0);
    if(dir == INVALID_HANDLE_VALUE) {
        if(GetLastError() != ERROR_FILE_NOT_FOUND) {
            ReportWindowsError(L"Can't FindFirstFile on %s\n", path.c_str());
        }
        return 0;
    }
    defer(FindClose(dir));

    do {
        wstr filename = $(L"%s\\%s", current_dir.c_str(), find_data.cFileName);
        wstr relative;
        if(!RelativePath(root_path, filename, relative)) {
            LOG_Error("Can't make a relative path from %s to %s", root_path.c_str(), filename.c_str());
            return 0;
        }
        if(find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
            if(ScanFolder(root_path, filename, specs) != 0) {
                entries_in_this_folder += 1;
            }
        }
    } while(FindNextFile(dir, &find_data) != 0);

    if(GetLastError() != ERROR_NO_MORE_FILES) {
        ReportWindowsError(L"Error scanning %s", path.c_str());
        return 0;
    }
    // record the folder if it's not empty (or they want empty ones), and not special (. or ..)
    if(!IsSpecial(relative_scan_dir)) {
        if(entries_in_this_folder == 0 && args.exclude_empty_folders) {
            LOG_Info("Excluding empty folder %s", relative_scan_dir.c_str());
        } else {
            folders.push_back(relative_scan_dir.c_str());
        }
    }
    return entries_in_this_folder;
}

//////////////////////////////////////////////////////////////////////

template <class container_t, class string_t, class char_t> void tokenize(string_t const &str, container_t &tokens, char_t const *delimiters, bool includeEmpty = true)
{
    string_t::size_type end = 0, start = 0, len = str.size();
    while(end < len) {
        end = str.find_first_of(delimiters, start);
        if(end == string_t::npos) {
            end = len;
        }
        if(end != start || includeEmpty) {
            tokens.push_back(container_t::value_type(str.data() + start, end - start));
        }
        start = end + 1;
    }
}

//////////////////////////////////////////////////////////////////////

wstr NullOrQuoted(wstr const &s)
{
    if(s.empty()) {
        return L"null";
    }
    return $(LR"("%s")", s.c_str());
}

//////////////////////////////////////////////////////////////////////

int wmain(int argc, wchar **argv)
{
    defer(if(IsDebuggerPresent()) {
        fprintf(stderr, "Done, press enter:");
        getchar();
    });

    LOG_Context("main");

    Log_SetOutputFiles(stdout, stderr);

    wstr current_folder = GetCurrentFolder();

    // convert the args to UTF8
    vector<string> narrow_args;
    narrow_args.reserve(argc);
    for(int i = 0; i < argc; ++i) {
        narrow_args.push_back(string(WStrToNarrow(argv[i])));
    }

    vector<char const *> narrow_argv;
    narrow_argv.reserve(argc);
    for(auto const &s : narrow_args) {
        narrow_argv.push_back(s.c_str());
    }

    if(!args.Init(argc, narrow_argv.data())) {
        fprintf(stderr, "%s\n", args.error_message.c_str());
        return ERROR_USAGE;
    }

    LOG_Level(args.verbosity);

    LOG_Verbose("Scanned arguments OK");

    if(!args.interim_folder.empty()) {

        LOG_Verbose("Interim folder is %s", args.interim_folder.c_str());

        // Delete the interim folder!!!
        if(args.clean_interim_folder) {

            LOG_Debug("Deleting %s", args.interim_folder.c_str());

            DeleteFolder(args.interim_folder.c_str());

            // (maybe re)create the interim folder
            if(!EnsureFolderExists(args.interim_folder)) {
                ReportWindowsError(L"Can't create interim-folder %s\n", args.interim_folder.c_str());
                return ERROR_CREATING_INTERIM_FOLDER;
            }
        }
    }

    wstr manifest_filename = PathJoin(args.interim_folder, args.manifest_name);

    LOG_Debug("Manifest file: %s", manifest_filename.c_str());

    int rc = OpenManifestFile(manifest_filename);
    if(rc != S_OK) {
        LOG_Error("Error opening %s for write", manifest_filename.c_str());
        return rc;
    }

    LOG_Verbose("Writing JSON top level");

    // top level bits
    WriteLNManifest(L"{");
    WriteLNManifest(LR"(	"application_name":"%s",)", args.application_name.c_str());
    WriteLNManifest(LR"(	"install_path":"%s",)", DoubleSlash(args.install_path).c_str());
    WriteLNManifest(LR"(	"max_downloads":%d,)", args.max_downloads);
    WriteLNManifest(LR"(	"launcher_arg":"%s",)", args.launcher_arg.c_str());
    WriteLNManifest(LR"(	"version":"%s",)", args.version.c_str());

    if(!args.min_version.empty()) {
        WriteLNManifest(LR"(	"min_version":"%s",)", args.min_version.c_str());
    }

    if(!args.max_version.empty()) {
        WriteLNManifest(LR"(	"max_version":"%s",)", args.max_version.c_str());
    }

    if(args.client_only) {
        WriteLNManifest(LR"(	"client_only":true,)");
    }

    if(args.server_only) {
        WriteLNManifest(LR"(	"server_only":true,)");
    }

    wstr working_folder = NullOrQuoted(args.working_folder);
    WriteLNManifest(LR"(	"working_dir":%s,)", DoubleSlash(working_folder).c_str());

    WriteLNManifest(LR"(	"exe":"%s",)", DoubleSlash(args.exe_file).c_str());

    if(!args.libraries.empty()) {

        vector<wstring> dlls;
        tokenize(wstring(args.libraries.c_str()), dlls, L",;", false);
        if(dlls.empty()) {
            LOG_Error("Can't get any strings from libraries arg!?");
            return ERROR_BAD_LIBRARIES_ARGUMENT;
        }
        if((dlls.size() % 2) != 0) {
            LOG_Error("odd # of strings (%d) in libraries!?", dlls.size());
            return ERROR_BAD_LIBRARIES_ARGUMENT;
        }

        LOG_Debug("%d library checks", dlls.size() / 2);

        WriteLNManifest(LR"(	"check_dlls":[)");
        wchar const *sep = L"\t\t";
        for(uint i = 0; i < dlls.size(); i += 2) {
            wstring name = dlls[i];
            wstring url = dlls[i + 1];

            LOG_Verbose("DLL Check: %s", name.c_str());

            WriteManifest(L"%s", sep);
            WriteLNManifest(L"{");
            WriteLNManifest(LR"(			"name":"%s",)", name.c_str());
            WriteLNManifest(LR"(			"url":"%s")", url.c_str());
            WriteManifest(L"\t\t}");
            sep = L",\n\t\t";
        }
        WriteLNManifest(L"\n\t],");
    }

    // grab the filespecs
    vector<wstring> specs;
    tokenize(wstring(args.filespecs.c_str()), specs, L",;", false);
    if(specs.empty()) {
        LOG_Warn("No filespecs, using *.*");
        specs.push_back(L"*.*");
    }

    // file scan
    wstr root_path = MakePathCanonical(args.source_folder);
    LOG_Info("Source folder: %s", root_path.c_str());
    LOG_Verbose("Filespecs: %s", args.filespecs.c_str());
    ScanFolder(root_path, root_path, specs);
    LOG_Info("%d files scanned (%lld bytes, %-4.2fMB)", files_scanned, total_files_size, total_files_size / 1048576.0);

    // output folders in reverse order because depth first traversal (because it needs to count entries in each folder)
    if(folders.size() > 0) {
        LOG_Info("%d folders", folders.size());
        WriteManifest(LR"(	"folders":[)");
        wchar const *sep = L"\n\t\t";
        for(int i = folders.size() - 1; i >= 0; --i) {
            wstring const &f = folders[i];
            LOG_Debug("Folder %s", f.c_str());
            WriteManifest(L"%s\"%s\"", sep, f.c_str());
            if(!args.interim_folder.empty()) {
                wstr root_interim_path = PathJoin(args.interim_folder, args.version.c_str());
                wstr folder_path = PathJoin(root_interim_path, f.c_str());
                EnsureFolderExists(folder_path);
            }
            sep = L",\n\t\t";
        }
        WriteLNManifest(L"\n\t],");
    }

    // output files
    if(files.size() > 0) {
        LOG_Info("%d files", files.size());
        WriteManifest(LR"(	"contents":[)");
        wchar const *sep = L"\n\t\t";
        for(File &f : files) {
            LOG_Debug("File %s", f.relative.c_str());
            WriteManifest(L"%s", sep);
            f.Output();
            sep = L",\n\t\t";
        }
        WriteLNManifest(L"\n\t]");
    }

    WriteLNManifest(L"}");

    LOG_Verbose("JSON output complete");

    // output the exe
    byte *exe_file;
    uint64 exe_size;
    if(!SUCCEEDED(LoadWinResource((uint32)IDR_RCDATA_EXEFILE, &exe_file, &exe_size))) {
        LOG_Error("Error loading exe resource");
        return ERROR_LOADING_EXE_RESOURCE;
    }
    LOG_Verbose("Loaded exe resource OK");

    if(!SUCCEEDED(SaveFile(args.launcher_filename, exe_file, exe_size))) {
        LOG_Error("Error saving launcher exe");
        return ERROR_SAVING_LAUNCHER_EXE;
    }
    LOG_Debug("Saved launcher exe %s", args.launcher_filename.c_str());

    // check url_base ends with a /, if not, add one
    if(args.url_base.back() != '/') {
        args.url_base = args.url_base.append(L"/");
    }

    files_folder = args.version + wstr(L"\\");

    // fiddle the resources in the launcher exe
    {
        LOG_Verbose("Updating launcher exe resources");

        HANDLE h = BeginUpdateResourceW(args.launcher_filename, false);
        if(h == INVALID_HANDLE_VALUE) {
            LOG_Error("Error updating exe resources (1)\n");
            return ERROR_UPDATING_LAUNCHER_RESOURCES;
        }
        scoped[&]
        {
            LOG_Verbose("Closing resource update handle");
            EndUpdateResource(h, false);
        };

        // string 1 is url_base, string 2 is manifest_name
        vector<wstr *> strings{
            null, &args.url_base, &args.manifest_name, &args.application_name, null, null, null, null, null, null, null, null, null, null, null, null,
        };

        vector<wchar> buffer;
        for(auto const p : strings) {
            if(p == null) {
                buffer.push_back(0);
            } else {
                wchar len = (wchar)p->size();
                buffer.push_back(len);
                size_t cur = buffer.size();
                buffer.resize(cur + len);
                memcpy(buffer.data() + cur, p->c_str(), len * sizeof(wchar));
            }
        }

        WORD language = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL);
        LPCWSTR string_resource_id = MAKEINTRESOURCE(IDS_URL_BASE);
        LPCWSTR icon_resource_id = MAKEINTRESOURCE(IDI_GERBIL);

        if(!UpdateResource(h, RT_STRING, string_resource_id, language, buffer.data(), buffer.size() * sizeof(WCHAR))) {
            LOG_Error("Error updating exe resources (2) %s", WindowsError(L"UpdateResource").c_str());
            return ERROR_UPDATING_LAUNCHER_RESOURCES;
        }
        LOG_Verbose("Updated string resources OK");

        // update the icon if there was one specified
        if(!args.icon_file.empty()) {
            MemoryBuffer<byte> icon_file;
            if(LoadFile(args.icon_file, icon_file) != S_OK) {
                LOG_Error("%s", WindowsError(L"Error loading %s as an icon", args.icon_file.c_str()).c_str());
                return ERROR_LOADING_ICON_FILE;
            }
            if(icon_file.size != 2238) {
                LOG_Error(".ico file should be 2238 bytes long (32x32, 8 bits per pixel, 1 bit mask), %s is %d bytes\n", args.icon_file.c_str(), icon_file.size);
                return ERROR_ICON_NOT_COMPATIBLE;
            }
            LOG_Verbose("Loaded %d bytes from %s", icon_file.size, args.icon_file.c_str());

            // magic offset 22 (seems wrong, I get 20 when I count up the fields, but whatever man)
            if(!UpdateResource(h, RT_ICON, icon_resource_id, language, (icon_file.data + 22), icon_file.size - 22)) {
                LOG_Error("Error updating exe resources (2)");
                return ERROR_UPDATING_LAUNCHER_RESOURCES;
            }
            LOG_Verbose("Updated icon resource OK");
        }
    }

    LOG_Info("gerbil complete");
    return 0;
}
