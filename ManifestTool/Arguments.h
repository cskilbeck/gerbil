#pragma once

#include <string>

struct Arguments
{
    enum
    {
        checksum_type_none,
        checksum_type_md5,
        checksum_type_sha256
    };

    using string = std::string;
    using wstring = std::wstring;

    wstr source_folder;
    wstr install_path;
    wstr exe_file;
    wstr working_folder;
    wstr filespecs;
    wstr application_name;
    wstr upgrade_from;
    wstr launcher_arg;
    wstr interim_folder;
    wstr icon_file;
    wstr libraries;
    wstr url_base;
    wstr version;
    wstr manifest_name;
    wstr min_version;
    wstr max_version;
    wstr launcher_filename;

    int checksum_type;
    int verbosity = 2;    // default verbosity is 'info'
    int max_downloads = default_max_downloads;

    bool clean_interim_folder;
    bool client_only;
    bool server_only;
    bool exclude_empty_folders;
    bool exclude_empty_files;

    string error_message;

    Arguments();

    bool Init(int argc, char const **argv);
};
