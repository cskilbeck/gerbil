//////////////////////////////////////////////////////////////////////

#include <sstream>
#include <vector>
#include "../common/common.h"
#include "optionparser.h"
#include "Arguments.h"

//////////////////////////////////////////////////////////////////////

LOG_Context("Arguments");

using std::vector;

//////////////////////////////////////////////////////////////////////

namespace
{

std::ostringstream errors;

using namespace option;
using option::ArgStatus;

//////////////////////////////////////////////////////////////////////

ArgStatus NotEmpty(const Option &option, bool msg)
{
    if(option.arg == null || option.arg[0] == 0)    // NotEmpty can't be used for ints which might be -ive
    {
        if(msg) {
            errors << "Missing value for option " << option.name << "\n";
        }
        return ArgStatus::ARG_ILLEGAL;
    }
    return ArgStatus::ARG_OK;
}

//////////////////////////////////////////////////////////////////////

ArgStatus Boolean(const option::Option &option, bool msg)
{
    if(option.arg == null) {
        return ArgStatus::ARG_NONE;
    }
    if(_strnicmp(option.arg, "false", 5) == 0 || _strnicmp(option.arg, "true", 4) == 0) {
        return ArgStatus::ARG_OK;
    }
    return ArgStatus::ARG_IGNORE;
}

//////////////////////////////////////////////////////////////////////

ArgStatus Unknown(const option::Option &option, bool msg)
{
    if(msg) {
        errors << "Unknown option '" << option.name << "'\n";
    }
    return option::ARG_ILLEGAL;
}

//////////////////////////////////////////////////////////////////////

ArgStatus ChecksumType(const option::Option &option, bool msg)
{
    if(option.arg == null || option.arg[0] == 0 || !(_strnicmp(option.arg, "md5", 3) == 0 || _strnicmp(option.arg, "sha256", 6) == 0)) {
        if(msg) {
            errors << "Bad argument for checksum-type, valid types are \"md5\" and \"sha256\"\n";
        }
        return ArgStatus::ARG_ILLEGAL;
    }
    return ArgStatus::ARG_OK;
}

//////////////////////////////////////////////////////////////////////

}    // namespace


using namespace option;

using option::ArgStatus;

enum optionIndex
{
    ARG_UNKNOWN,
    ARG_HELP,
    ARG_SOURCE_FOLDER,
    ARG_INSTALL_PATH,
    ARG_EXE_FILE,
    ARG_WORKING_FOLDER,
    ARG_APPLICATION_NAME,
    ARG_LAUNCHER_FILENAME,
    ARG_URL_BASE,
    ARG_VERSION,
    ARG_CHECKSUM_TYPE,
    ARG_MANIFEST_NAME,
    ARG_UPGRADE_FROM,
    ARG_MIN_VERSION,
    ARG_MAX_VERSION,
    ARG_MAX_DOWNLOADS,
    ARG_INTERIM_FOLDER,
    ARG_CLEAN_INTERIM_FOLDER,
    ARG_FILESPECS,
    ARG_LAUNCHER_ARG,
    ARG_LIBRARIES,
    ARG_ICON_FILE,
    ARG_VERBOSITY,
    ARG_CLIENT_ONLY,
    ARG_SERVER_ONLY,
    ARG_EXCLUDE_EMPTY_FOLDERS,
    ARG_EXCLUDE_EMPTY_FILES,

    ARG_MAX_OPTIONS
};

static str max_downloads_description =
    $("  --max-downloads integer(1..32) \tOptional. Max concurrent downloads to allow during install, default is %d", max_max_downloads, default_max_downloads);

Descriptor usage[] = {

    // usage/help
    { ARG_UNKNOWN, 0, "", "", Unknown, "USAGE: gerbil [options]\n\nOptions:" },

    { ARG_HELP, 0, "", "help", Arg::None, "  --help  \tPrint usage and exit." },

    // mandatory
    { ARG_SOURCE_FOLDER, 0, "", "source-folder", NotEmpty, "  --source-folder path \tWhere the files to be installed are located" },

    { ARG_INSTALL_PATH, 0, "", "install-path", NotEmpty,
      "  --install-path path  \tWhere the application will be installed on the target machine. Environment variables will be expanded at runtime" },

    { ARG_EXE_FILE, 0, "", "exe-file", NotEmpty, "  --exe-file filename \tThe file which will be launched after a successfull installation" },

    { ARG_APPLICATION_NAME, 0, "", "application-name", NotEmpty, "  --application-name string \tUser friendly application display name" },

    { ARG_LAUNCHER_FILENAME, 0, "", "launcher-filename", NotEmpty, "  --launcher-filename filename \tWhere to save the downloader executable" },

    { ARG_URL_BASE, 0, "", "url-base", NotEmpty, "  --url-base string \tURL base of manifest and files location" },

    { ARG_VERSION, 0, "", "version", NotEmpty, "  --version string/path \tVersion (actually, folder name where install files will be stored on the server)" },

    // optional
    { ARG_MIN_VERSION, 0, "", "min-version", NotEmpty, "  --min-version version \tOptional. Min windows version supported, default is any version" },

    { ARG_MAX_VERSION, 0, "", "max-version", NotEmpty, "  --max-version version \tOptional. Max windows version supported, default is any version" },

    { ARG_WORKING_FOLDER, 0, "", "working-folder", NotEmpty,
      "  --working-folder path \tOptional. Working folder to be set when launching the application (relative to install-path), defaults to install-path" },

    { ARG_CHECKSUM_TYPE, 0, "", "checksum-type", ChecksumType, "  --checksum-type md5/sha256 \tOptional. Set checksum type, default is sha256 (md5 is deprecated)" },

    { ARG_MANIFEST_NAME, 0, "", "manifest-name", NotEmpty, "  --manifest-name string \tOptional. Name of manifest file on the server, default is manifest.gerbil" },

    { ARG_MAX_DOWNLOADS, 0, "", "max-downloads", NotEmpty, max_downloads_description.c_str() },

    { ARG_INTERIM_FOLDER, 0, "", "interim-folder", NotEmpty, "  --interim-folder path \tOptional. If specified, all scanned files will be copied to this folder" },

    { ARG_CLEAN_INTERIM_FOLDER, 0, "", "clean-interim-folder", Boolean,
      "  --clean-interim-folder \tOptional. Flag. If specified, and interim-folder is specified, interim-folder will be cleaned before files are copied into it" },

    { ARG_FILESPECS, 0, "", "filespecs", NotEmpty,
      "  --filespecs speclist \tOptional. Semicolon separated list of filespecs to include in the installation (eg \"*.exe;*.dll\"), default is *.*" },

    { ARG_LAUNCHER_ARG, 0, "", "launcher-arg", NotEmpty,
      "  --launcher-arg argstring \tOptional. Args that will be passed to the exe when it is run, you can use the special macro ${LAUNCHER_PATH} to denote the installer exe" },

    { ARG_LIBRARIES, 0, "", "libraries", NotEmpty,
      "  --libraries liblist \tOptional. Semicolon separated list of string pairs where the 1st is the name of a DLL (eg d3d9.dll) and the 2nd is the url to launch if it's not "
      "there at install time" },

    { ARG_ICON_FILE, 0, "", "icon-file", NotEmpty,
      "  --icon-file filename \tOptional. Icon file to embed into the launcher (must be a 32x32, 8 bit palette, 1 bit alpha .ico file)" },

    { ARG_VERBOSITY, 0, "", "verbosity", NotEmpty, "  --verbosity integer(0-4) \tOptional. 0 = errors, 1 = warnings, 2 = info, 3 = debug, 4 = verbose, default is 2 (info)" },

    { ARG_CLIENT_ONLY, 0, "", "client-only", Boolean, "  --client-only \tOptional. Flag. If specified, application will run only on client versions of Windows" },

    { ARG_SERVER_ONLY, 0, "", "server-only", Boolean, "  --server-only \tOptional. Flag. If specified, application will run only on server versions of Windows" },

    { ARG_EXCLUDE_EMPTY_FOLDERS, 0, "", "exclude-empty-folders", Boolean,
      "  --exclude-empty-folders \tOptional. Flag. If specified, empty folders will not be included in the manifest" },

    { ARG_EXCLUDE_EMPTY_FILES, 0, "", "exclude-empty-files", Boolean,
      "  --exclude-empty-files  \tOptional. Flag. If specified, empty files (0 bytes length) will not be included in the manifest" },

    { 0, 0, 0, 0, 0, 0 }
};

//////////////////////////////////////////////////////////////////////

Descriptor *FindDesc(int id)
{
    for(int i = 0; i < ARG_MAX_OPTIONS; ++i) {
        if(usage[i].index == id) {
            return usage + i;
        }
    }
    return null;
}

//////////////////////////////////////////////////////////////////////

Arguments::Arguments()
{
}

//////////////////////////////////////////////////////////////////////

bool Arguments::Init(int argc, char const **argv)
{
    errors.clear();
    errors << "Error parsing arguments:\n";

    Stats stats(usage, argc - 1, argv + 1);
    vector<Option> options(stats.options_max);
    vector<Option> buffer(stats.buffer_max);
    Parser parse(usage, argc - 1, argv + 1, options.data(), buffer.data());

    // explicitly check everything
    // gather all errors before reporting

    bool args_ok = !parse.error();

    if(args_ok) {

        auto MandatoryStr = [&](int id, wstr &s) -> void {
            if(options[id]) {
                s = StrToWide(options[id].arg);
                if(s.empty()) {
                    errors << "Missing value for " << options[id].desc->longopt << "\n";
                    args_ok = false;
                }
            } else {
                Descriptor *d = FindDesc(id);
                errors << "Missing parameter: " << d->longopt << "\n";
                args_ok = false;
            }
        };

        auto OptionalStr = [&](int id, wstr &s) {
            if(options[id]) {
                if(options[id].arg != null) {
                    s = StrToWide(options[id].arg);
                } else {
                    errors << "Missing value for " << options[id].desc->longopt << "\n";
                    args_ok = false;
                }
            }
        };

        auto OptionalInt = [&](int id, int &s) {
            if(options[id]) {
                if(options[id].arg != null) {
                    char *ender;
                    s = strtol(options[id].arg, &ender, 10);
                    if(ender == options[id].arg) {
                        errors << "Bad integer for " << options[id].desc->longopt << "\n";
                    }
                } else {
                    errors << "Missing value for " << options[id].desc->longopt << "\n";
                }
            }
        };

        auto OptionalBool = [&](int id, bool &b) {
            b = options[id] && (options[id].arg == null || strnlen(options[id].arg, 5) == 4 && _strnicmp(options[id].arg, "true", 4) == 0);
        };

        // default values

        verbosity = 2;
        manifest_name = L"manifest.gerbil";
        max_downloads = default_max_downloads;
        clean_interim_folder = false;
        client_only = false;
        server_only = false;
        exclude_empty_folders = false;
        exclude_empty_files = false;
        filespecs = L"*.*";

        // grab/check values

        wstr checksum_type_str;    // enum so need a temp. string

        MandatoryStr(ARG_SOURCE_FOLDER, source_folder);
        MandatoryStr(ARG_INSTALL_PATH, install_path);
        MandatoryStr(ARG_EXE_FILE, exe_file);
        MandatoryStr(ARG_APPLICATION_NAME, application_name);
        MandatoryStr(ARG_LAUNCHER_FILENAME, launcher_filename);
        MandatoryStr(ARG_URL_BASE, url_base);
        MandatoryStr(ARG_VERSION, version);

        OptionalStr(ARG_MANIFEST_NAME, manifest_name);
        OptionalStr(ARG_CHECKSUM_TYPE, checksum_type_str);
        OptionalStr(ARG_LIBRARIES, libraries);
        OptionalStr(ARG_INTERIM_FOLDER, interim_folder);
        OptionalStr(ARG_MIN_VERSION, min_version);
        OptionalStr(ARG_ICON_FILE, icon_file);
        OptionalStr(ARG_FILESPECS, filespecs);

        OptionalStr(ARG_MAX_VERSION, max_version);
        OptionalStr(ARG_WORKING_FOLDER, working_folder);
        OptionalStr(ARG_UPGRADE_FROM, upgrade_from);
        OptionalStr(ARG_LAUNCHER_ARG, launcher_arg);

        OptionalInt(ARG_VERBOSITY, verbosity);
        OptionalInt(ARG_MAX_DOWNLOADS, max_downloads);

        OptionalBool(ARG_CLEAN_INTERIM_FOLDER, clean_interim_folder);
        OptionalBool(ARG_CLIENT_ONLY, client_only);
        OptionalBool(ARG_SERVER_ONLY, server_only);
        OptionalBool(ARG_EXCLUDE_EMPTY_FILES, exclude_empty_files);
        OptionalBool(ARG_EXCLUDE_EMPTY_FOLDERS, exclude_empty_folders);

        // validation phase

        // shitty hard coded strings for checksum-type for now
        checksum_type = checksum_type_sha256;
        if(!checksum_type_str.empty()) {
            if(_wcsicmp(checksum_type_str, L"md5") == 0) {
                checksum_type = checksum_type_md5;
            } else if(_wcsicmp(checksum_type_str, L"sha256") == 0) {    // redundant, but there might be others later
                checksum_type = checksum_type_sha256;
            } else {
                errors << "Unknown checksum-type, valid types are md5, sha256\n";
                args_ok = false;
            }
        }

        if(max_downloads < 1 || max_downloads > max_max_downloads) {
            errors << "max_downloads " << max_downloads << "out of range, can be 1.." << max_max_downloads << "\n";
            args_ok = false;
        }

        if(client_only && server_only) {
            errors << "Can't specify client-only AND server-only\n";
            args_ok = false;
        }

        if(verbosity < 0 || verbosity > 4) {
            errors << "Bad verbosity level: %d, can be 0 (errors only) to 4 (verbose)\n";
            args_ok = false;
        }
    }

    // show help if requested
    // add the usage if there was an error
    std::ostringstream help_string;
    if(options[ARG_HELP] || !args_ok) {
        help_string << errors.str() << "\n";
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
        int columns = csbi.srWindow.Right - csbi.srWindow.Left + 1;
        printUsage(help_string, usage, columns);
        error_message = help_string.str();
        return false;
    }
    return true;
}
