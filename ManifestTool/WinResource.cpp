//////////////////////////////////////////////////////////////////////

#include "../common/common.h"
#include "WinResource.h"

//////////////////////////////////////////////////////////////////////
// Load a binary resource from the exe (and leak it for now)

HRESULT LoadWinResource(uint32 resourceid, byte **data, uint64 *size)
{
    HRSRC myResource = ::FindResource(NULL, MAKEINTRESOURCE(resourceid), RT_RCDATA);
    if(myResource == null) {
        return HRESULT_FROM_WIN32(ERROR_RESOURCE_DATA_NOT_FOUND);
    }

    HGLOBAL myResourceData = ::LoadResource(NULL, myResource);
    if(myResourceData == null) {
        return HRESULT_FROM_WIN32(ERROR_RESOURCE_FAILED);
    }

    byte *pMyBinaryData = reinterpret_cast<byte *>(::LockResource(myResourceData));
    if(pMyBinaryData == null) {
        return HRESULT_FROM_WIN32(ERROR_RESOURCE_NOT_AVAILABLE);
    }

    uint64 s = (uint64)SizeofResource(GetModuleHandle(null), myResource);
    if(s == 0) {
        return HRESULT_FROM_WIN32(ERROR_RESOURCE_FAILED);
    }

    if(size != null) {
        *size = s;
    }
    if(data != null) {
        *data = pMyBinaryData;
    }
    return S_OK;
}
