//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Gerbil.rc
//
#define IDI_GERBIL                      1
#define IDS_URL_BASE                    1
#define IDC_MYICON                      2
#define IDS_MANIFEST_NAME               2
#define IDS_APP_NAME                    3
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           112
#endif
#endif
