//////////////////////////////////////////////////////////////////////
// TODO (chs): normalize fonts between Gerbil launcher and native client
// TODO (chs): test Gerbil upgrade flow
//
// TODO (chs): configurable telemetry thing as well as error messages (it pings an endpoint specified in the manifest?)
// TODO (chs): save a copy of the manifest, then, when a new one comes in, scan for old ones and delete old versions (after successfully downloading the new one)
// TODO (chs): messagebox (or...?) if they try to run it, and it needs updating and it's already running...
// TODO (chs): localisation... (or... use icons for everything, esp. network error status)
// TODO (chs): empty folders are not recreated in the install - does this matter?

// TEST : proxy support
// TEST : SSL
// TEST : OS version check
// TEST : str, wstr
// TEST : all FatalError conditions

// DONE (chs): fix quit/cancel shutdown bug (for now, might be a little race in there somewhere...)
// DONE (chs): try and repro that weird checksum thing (broken constructor chain?)
// DONE (chs): add exclude-empty-folders and exclude-empty-files options
// DONE (chs): fix 0 byte file size (just create it?)
// DONE (chs): recreate folder structure (need a 'folder' element?)
// DONE (chs): add SHA256 support (but continue to support legacy MD5 manifest files)
// DONE (chs): launcher logs to temp folder
// DONE (chs): support client/server requirement args
// DONE (chs): update readme about version arg
// DONE (chs): fixup logging situation in ManifestTool
// DONE (chs): add \n \t etc handling to json string parser
// DONE (chs): copy assets to an interim folder for uploading
// DONE (chs): ensure it quits under all error conditions
// DONE (chs): support OS version check in the manifest
// DONE (chs): replace icon in the launcher
// DONE (chs): simple upgrade path for upgrading the installer
// DONE (chs): option in the manifest to display a message
// DONE (chs): option in the manifest to quit, rather than running or downloading anything
// DONE (chs): scale window and progress bar by DPI (using shitty apis which are available on Windows XP)
// DONE (chs): add quit_message and quit_url for when the app is no longer available
// DONE (chs): center window in default monitor
// DONE (chs): merge Download and ProgressDownloader into InstallDownloader
// DONE (chs): put OS version check in the manifest
// DONE (chs): make arg parser work properly when it encounters an unknown arg
// DONE (chs): manifest generator outputs a customized installer.exe (bake in resource strings for urls etc)
// DONE (chs): json token array allocated dynamically
// DONE (chs): proper arg parser on the manifest generator
// DONE (chs): check if directX is installed (add 'dlls' section to manifest, each has name of dll and url to pop if missing)
// DONE (chs): put IDS_LAUNCHPARAMETER in the manifest
// DONE (chs): put max_downloads in the manifest as optional, default = 4
// DONE (chs): create window on demand (kinda, it shows it if it needs to)
// DONE (chs): progress bar if total size > some amount
// DONE (chs): put IDS_INSTALLEDAPPLICATION in the manifest, use exe name until manifest downloaded
// DONE (chs): OS version check
// DONE (chs): make it support proxy on Windows XP and up (which is a right hassle)
// DONE (chs): urlescape the manifest url (not done)
// DONE (chs): finish error checking/reporting (although it's an ongoing thing)
// DONE (chs): finish the readme
// DONE (chs): max simultaneous downloads
// DONE (chs): check HTTPS support
// DONE (chs): calc MD5 as data comes in
// DONE (chs): check MD5 of existing files
// DONE (chs): downloader grabs response into error_message if status_code is bad
// DONE (chs): path canonicalize
// DONE (chs): move common files into their own folder
// DONE (chs): make it fully wchar
// DONE (chs): total size of all downloads in the manifest
// DONE (chs): create folders as necessary when downloading files
// DONE (chs): command line app which generates manifest
// DONE (chs): add --gerbil-path "xxxx" to parameters of launched application
// DONE (chs): widen utf-8 json strings
// DONE (chs): logging wide/narrow
// DONE (chs): logging
// DONE (chs): make it threaded (and cancellable)
// DONE (chs): make a version which downloads to a buffer
// DONE (chs): crack the url so you can give it http://something.com:80

// NOPE (chs): differential update: show it where the previous version is, and put new/changed files into another folder (but, the value of this is dubious, because any files which
//             haven't changed wont be downloaded anyway, so... worth it? all it does is save a bit of space on the server really)
// NOPE (chs): make it output the json properly rather than hard coding it
// NOPE (chs): ditch the cancel event, that's what the close window button is for, we don't need a button (wait, we do, if the network traffic hangs)
// NOPE (chs): busy mouse while it's downloading somehow?
// NOPE (chs): cancel button if progress bar showing
// NOPE (chs): authentication? why?

#include "../common/common.h"
#include "../Common/md5.h"
#include "../Common/json.h"
#include "gerbil_resource.h"
#include "Downloader.h"

#include <shellapi.h>

#pragma comment(lib, "winhttp.lib")
#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "comctl32.lib")
#pragma comment(lib, "winmm.lib")

LOG_Context("Gerbil");

//////////////////////////////////////////////////////////////////////

HINSTANCE hInst;
bool window_created = false;
HWND main_window = null;
HWND progress_bar = null;
HWND message_text = null;
HWND button = null;
HANDLE cancel_event = INVALID_HANDLE_VALUE;
HANDLE quit_event = INVALID_HANDLE_VALUE;
CRITICAL_SECTION quit_critical_section;
wstr application_name;
wstr url_base;
uint max_downloads = default_max_downloads;
wstr quit_url;
uint64 total_download_size = 0;
uint64 download_progress = 0;
HBRUSH static_background_brush;
LOGFONT static_logfont{ 22, 0, 0, 0, FW_EXTRALIGHT, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, VARIABLE_PITCH | FF_SWISS };
HFONT static_font;
int text_margin_x = 6;
int text_margin_y = 6;
RECT message_rect{ 200, 200, 200 + 800, 200 + 200 };
RECT progress_rect{ 200, 200, 200 + 200, 200 + 24 };
DWORD foreground_color = RGB(140, 140, 140);
DWORD background_color = RGB(40, 40, 40);

class Download;

Download *downloads = null;
int file_count = 0;

constexpr uint32 BUTTON_ID = 100;

void CancelAllDownloads();

//////////////////////////////////////////////////////////////////////

bool DidQuit()
{
    return WaitForSingleObject(quit_event, 0) == WAIT_OBJECT_0;
}

//////////////////////////////////////////////////////////////////////
// FatalError

bool Quit()
{
    EnterCriticalSection(&quit_critical_section);
    bool rc = DidQuit();
    SetEvent(quit_event);
    CancelAllDownloads();    // array of downloads has to be global for this, but it would make life easier
    LeaveCriticalSection(&quit_critical_section);
    return rc;
}

//////////////////////////////////////////////////////////////////////

void Cancel()
{
    SetEvent(cancel_event);
}

//////////////////////////////////////////////////////////////////////

wstr LoadResourceString(DWORD id)
{
    wchar *ptr[2];    // docs say 8 bytes required (really? in win32?)
    LoadStringW(hInst, id, (LPWSTR)&ptr, 0);
    // it turns out that the preceding wide char is the length...
    // for now, use it, but needs to be done properly at some point
    int len = ptr[0][-1];
    wstr result(len + 1);
    memcpy(result.buffer(), ptr[0], len * sizeof(wchar));
    result.buffer()[len] = 0;
    return result;
}

//////////////////////////////////////////////////////////////////////

void ScaleRectByCurrentDPI(RECT &rc)
{
    int w = rc.right - rc.left;
    int h = rc.bottom - rc.top;
    HDC dc = GetDC(null);
    int dpi_x = GetDeviceCaps(dc, LOGPIXELSX);
    int dpi_y = GetDeviceCaps(dc, LOGPIXELSX);    // redundant, always == dpi_x apparently
    ReleaseDC(null, dc);
    w = w * dpi_x / 96;
    h = h * dpi_y / 96;
    rc.right = rc.left + w;
    rc.bottom = rc.top + h;
}

//////////////////////////////////////////////////////////////////////

void CenterRectInDefaultMonitor(RECT &rc)
{
    MONITORINFOEX monitor_info;
    monitor_info.cbSize = sizeof(monitor_info);
    HMONITOR default_monitor = MonitorFromPoint({ 0, 0 }, MONITOR_DEFAULTTOPRIMARY);
    if(default_monitor != null) {
        if(GetMonitorInfo(default_monitor, &monitor_info)) {
            RECT &m = monitor_info.rcWork;
            int w = rc.right - rc.left;
            int h = rc.bottom - rc.top;
            rc.left = (m.right + m.left) / 2 - w / 2;
            rc.top = (m.bottom + m.top) / 2 - h / 2;
            rc.right = rc.left + w;
            rc.bottom = rc.top + h;
        }
    }
}

//////////////////////////////////////////////////////////////////////

void ShowMainWindow()
{
    ShowWindowAsync(main_window, SW_SHOW);
}

//////////////////////////////////////////////////////////////////////

void CloseMainWindow()
{
    PostMessage(main_window, WM_CLOSE, 0, 0);
}

//////////////////////////////////////////////////////////////////////

void ShowProgressBar()
{
    ShowWindowAsync(progress_bar, SW_SHOW);
}

//////////////////////////////////////////////////////////////////////

void SetButtonText(wchar const *text)
{
    SetWindowText(button, text);
    ShowWindowAsync(button, SW_SHOW);
}

//////////////////////////////////////////////////////////////////////

void MaximizeChildWindow(HWND child_window, LPARAM lParam)
{
    SetWindowPos(child_window, null, text_margin_x, text_margin_y, LOWORD(lParam) - text_margin_x * 2, HIWORD(lParam) - text_margin_y * 2, SWP_NOZORDER | SWP_ASYNCWINDOWPOS);
}

//////////////////////////////////////////////////////////////////////

void AlignButton(LPARAM lParam)
{
    int x = LOWORD(lParam);
    int y = HIWORD(lParam);
    RECT button_rect;
    GetWindowRect(button, &button_rect);
    int w = button_rect.right - button_rect.left;
    int h = button_rect.bottom - button_rect.top;
    x = x - (w + text_margin_x);
    y = y - (h + text_margin_y);
    SetWindowPos(button, null, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_ASYNCWINDOWPOS);
    BringWindowToTop(button);
}

//////////////////////////////////////////////////////////////////////

void SetMessageAlign(uint32 align)
{
    uint32 style = GetWindowLong(message_text, GWL_STYLE);
    style &= ~SS_TYPEMASK;
    style |= align;
    SetWindowLong(message_text, GWL_STYLE, style);
}

//////////////////////////////////////////////////////////////////////

void SetMessageText(wstr const &message)
{
    ShowWindowAsync(progress_bar, SW_HIDE);
    if(message_text != null) {
        ShowWindowAsync(message_text, SW_SHOW);
        SetWindowText(message_text, message);
    }
    RECT rc = message_rect;
    ScaleRectByCurrentDPI(rc);
    CenterRectInDefaultMonitor(rc);
    SetWindowPos(main_window, null, rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top, SWP_NOZORDER | SWP_SHOWWINDOW | SWP_ASYNCWINDOWPOS);
    ShowMainWindow();
}

//////////////////////////////////////////////////////////////////////

void FatalError(wchar const *msg, ...)
{
    if(Quit()) {
        return;
    }

    va_list v;
    va_start(v, msg);
    wstr text = wstr::format(msg, v);

    LOG_Error("%s\n", text.c_str());

    if(window_created) {
        SetMessageAlign(SS_LEFT);
        SetMessageText(text);
        SetButtonText(L"Quit");
    } else {
        MessageBoxW(null, text, application_name, MB_ICONEXCLAMATION);
    }
    Cancel();
}

//////////////////////////////////////////////////////////////////////

void WindowsErrorMessage(wchar const *message, ...)
{
    va_list v;
    va_start(v, message);
    wstr msg = $(L"Error:%s\n", GetWindowsError(GetLastError(), message, v).c_str());
    LOG_Error("%s\n", msg.c_str());
    FatalError(L"%s", msg.c_str());
}

//////////////////////////////////////////////////////////////////////

void LaunchURL(wstr const &url)
{
    UrlComponents c;    // bit lame, check it's a URL before launching it
    if(c.Init(url)) {
        HINSTANCE launched = ShellExecuteW(null, L"OPEN", url, null, null, SW_SHOWNORMAL);
        if(launched <= (HINSTANCE)32) {
            FatalError(L"Failed to launch url (error %d): %s", (int)launched, url.c_str());
        }
    } else {
        FatalError(L"Bad url: %s", url.c_str());
    }
}

//////////////////////////////////////////////////////////////////////

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message) {

    case WM_DESTROY:
        PostQuitMessage(0);
        break;

    case WM_CLOSE:
        Cancel();
        DestroyWindow(main_window);
        break;

    case WM_CTLCOLORSTATIC: {
        HDC dc = (HDC)wParam;
        SetTextColor(dc, foreground_color);
        SetBkColor(dc, background_color);
        return (LRESULT)static_background_brush;
    } break;

    case WM_SIZE:
        MaximizeChildWindow(message_text, lParam);
        MaximizeChildWindow(progress_bar, lParam);
        AlignButton(lParam);
        break;

    case WM_COMMAND:
        switch(wParam) {
        case BUTTON_ID:
            CloseMainWindow();
            break;
        }
        break;

    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

//////////////////////////////////////////////////////////////////////
// get a string from a JsonValue with checking

static bool GetNarrowStringFromJsonValue(JsonValue const &v, str &result)
{
    if(v.IsValid() && v.getType() == JsonType::String) {
        size_t len = v.getTokenLength();
        if(len != 0) {
            result.resize(len);
            v.getString(result.buffer(), len + 1);
            result.buffer()[len] = 0;
            result = ReplaceStr(result, str("\\n"), str("\n"));
            result = ReplaceStr(result, str("\\t"), str("\t"));
            result = ReplaceStr(result, str("\\\\"), str("\\"));
        }
        return true;
    }
    return false;
}

//////////////////////////////////////////////////////////////////////
// get a string from a JsonValue with checking

static bool GetWideStringFromJsonValue(JsonValue const &v, wstr &result)
{
    str s;
    if(!GetNarrowStringFromJsonValue(v, s)) {
        return false;
    }
    result = StrToWide(s);
    return true;
}

//////////////////////////////////////////////////////////////////////

static bool GetBoolFromJsonValue(JsonValue const &v, bool &result)
{
    if(v.IsValid() && v.getType() == JsonType::Boolean) {
        result = v.getBool();
        return true;
    }
    return false;
}

//////////////////////////////////////////////////////////////////////

template <typename T> static bool GetIntFromJsonValue(JsonValue const &v, T &result)
{
    if(v.IsValid() && v.getType() == JsonType::Number) {
        result = (T)v.getInt64();
        return true;
    }
    return false;
}

//////////////////////////////////////////////////////////////////////

static wstr UrlJoin(wstr const &a, wstr const &b)
{
    DWORD len;
    wchar combined[4096];
    UrlCombineW(a, b, combined, &len, 0);
    return combined;
}

//////////////////////////////////////////////////////////////////////
// File download request admin thingy for keeping track of what needs downloading
// and update the progress bar as they arrive

class Download : public FileDownloader
{
public:
    LOG_Context("Download");

    Download() : FileDownloader()
    {
    }

    ~Download()
    {
    }

    // this returns false if there was a proper error, not if the value is just missing, which may not matter
    template <typename T> bool InitChecksum(JsonValue &p, wstr const &filename)
    {
        JsonValue &v = p[T::name];
        if(!v.IsValid()) {
            return true;
        }
        if(checksum != null) {
            LOG_Error("Multiple checksums supplied for %s", filename.c_str());
            return false;
        }
        str s;
        if(GetNarrowStringFromJsonValue(v, s)) {
            byte sum[T::size];
            if(s.size() != T::size * 2 || !HexStringToBytes(s, T::size * 2, sum)) {
                LOG_Error("Bad %s string %s", T::name, s.c_str());
                return false;
            }
            SetChecksum(new T(), sum);
        }
        return true;
    }

    bool InitFromJsonValue(JsonValue v, wstr const &install_path, wstr const &version)
    {
        if(!GetWideStringFromJsonValue(v["file"], filename)) {
            LOG_Error("Bad path");
            return false;
        }

        if(!GetIntFromJsonValue(v["size"], size)) {
            LOG_Error("Bad size");
            size = 0;
            return false;
        }

        if(size != 0) {
            checksum = null;

            if(!InitChecksum<Hash::MD5>(v, filename)) {
                return false;
            }
            if(!InitChecksum<Hash::SHA256>(v, filename)) {
                return false;
            }

            if(checksum == null) {
                FatalError(L"Missing checksum for %s", filename.c_str());
                return false;
            }
            LOG_Debug("Checksum for %s is %s", filename.c_str(), BytesToHexString(required_checksum, checksum->Size()).c_str());
        } else {
            LOG_Debug("File %s is empty...", filename.c_str());
        }

        wstr url_base_from = UrlJoin(url_base, version).append(L"/");
        download_url = UrlJoin(url_base_from, ForwardSlashes(filename));
        Init(PathJoin(install_path, filename));

        needs_downloading = !Exists() || (size == 0);
        if(needs_downloading) {
            LOG_Debug("Need %s (%d bytes)", filename.c_str(), size);
        } else {
            LOG_Debug("Already got %s", filename.c_str());
        }
        return true;
    }

    bool Exists()
    {
        if(!FileExists(filename)) {
            return false;
        }

        // no checksum has been set, file existence is enough
        if(checksum == null) {
            return true;
        }

        // checksum object was set up, use it to calculate file checksum
        uint64_t file_size;
        if(!ChecksumFile(filename, file_size, checksum)) {
            WindowsErrorMessage(L"Can't checksum %s\n", filename.c_str());
            return false;
        }
        return memcmp(checksum->Result(), required_checksum, checksum->Size()) == 0;
    }

    bool Start()
    {
        checksum->Init();
        if(!Downloader::Start(download_url)) {
            FatalError(L"Error getting %s from %s\n\n%s", filename.c_str(), Url().c_str(), ErrorMessage().c_str());
            return false;
        }
        return true;
    }

    bool OnReceiveComplete() override
    {
        if(!FileDownloader::OnReceiveComplete()) {
            FatalError(L"Can't download %s\n\n%s", download_url.c_str(), error_message.c_str());
            Cancel();
            return false;
        }
        return true;
    }

    bool OnReceiveData(byte *data, size_t size) override
    {
        if(DidQuit()) {
            return true;
        }
        bool drawit = false;

        if(download_progress == 0) {
            ShowMainWindow();
            ShowProgressBar();
            drawit = true;
        }
        download_progress += size;
        if(download_progress > total_download_size) {
            download_progress = total_download_size;
            drawit = true;
        }
        // only update the progress bar every 1/5th second to stop flickering
        if(timeGetTime() - last_draw_time > 200) {
            drawit = true;
        }

        if(drawit) {
            PostMessage(progress_bar, PBM_SETPOS, (WPARAM)download_progress, 0);
            last_draw_time = timeGetTime();
        }
        return FileDownloader::OnReceiveData(data, size);
    }

    void OnFailure() override
    {
        FatalError(L"Can't download %s\n\n%s", download_url.c_str(), error_message.c_str());
        Cancel();
    }

    bool needs_downloading = false;
    wstr download_url;
    DWORD last_draw_time = 0;
    int64_t size = 0;
};

//////////////////////////////////////////////////////////////////////

void CancelAllDownloads()
{
    for(int i = 0; i < file_count; ++i) {
        downloads[i].Terminate();
    }
}

//////////////////////////////////////////////////////////////////////
// MemoryDownloader for downloading the manifest and then all the files

class ManifestDownloader : public MemoryDownloader
{
    LOG_Context("ManifestDownloader");

public:
    ManifestDownloader() : MemoryDownloader()
    {
    }

    ~ManifestDownloader()
    {
    }

    // return true to indicate window should be closed
    void DownloadFiles()
    {
        wstr launcher_arg;
        wstr install_path;
        wstr working_dir;
        wstr exe_to_run;
        JsonToken *tokens;
        size_t token_count;

        // parse the json
        auto k = jsonTokenize((char const *)Data(), Size() - 1, null, 0, token_count);
        if(k != 0) {
            FatalError(L"Invalid config detected (-1)");
            return;
        }
        tokens = new JsonToken[token_count];
        defer(delete[] tokens);
        jsonTokenize((char const *)Data(), Size(), tokens, token_count, token_count);

        JsonValue json = JsonValue::parse(tokens);
        if(!json.IsValid()) {
            FatalError(L"Invalid config detected (0)");
            return;
        }

        // get the top level elements

        // application_name
        if(!GetWideStringFromJsonValue(json["application_name"], application_name)) {
            LOG_Error("No application name in manifest");
            FatalError(L"Invalid config detected (1)");
            return;
        }
        SendMessage(main_window, WM_SETTEXT, 0, (LPARAM)application_name.c_str());

        wstr version;
        if(!GetWideStringFromJsonValue(json["version"], version)) {
            LOG_Error("No version in manifest");
            FatalError(L"Invalid config detected (1)");
            return;
        }

        // does it want to launch a url when it quits?
        GetWideStringFromJsonValue(json["quit_url"], quit_url);

        // does it have a special message to show?
        wstr message;
        if(GetWideStringFromJsonValue(json["quit_message"], message)) {
            SetButtonText(L"OK");
            SetMessageText(ReplaceStr(message, wstr(L"\\n"), wstr(L"\n")));
            return;
        }

        // does it need a certain version of Windows?
        wstr min_version;
        bool client_only = false;
        bool server_only = false;
        GetWideStringFromJsonValue(json["min_version"], min_version);
        GetBoolFromJsonValue(json["client_only"], client_only);
        GetBoolFromJsonValue(json["server_only"], server_only);
        if(!min_version.empty() || client_only || server_only) {
            bool is_server = false;
            int current_version = GetCurrentWindowsVersion(is_server);
            wstr current_version_name = ConvertWindowsVersionToString(current_version, is_server);
            if(client_only && is_server) {
                FatalError(L"%s cannot run on %s, it requires a Windows Client Operating System", application_name.c_str(), current_version_name.c_str());
                return;
            }
            if(server_only && !is_server) {
                FatalError(L"%s cannot run on %s, it requires a Windows Server Operating System", application_name.c_str(), current_version_name.c_str());
                return;
            }
            if(!min_version.empty()) {
                int required_version = ConvertStringToWindowsVersion(min_version);
                if(required_version != 0 && current_version < required_version) {
                    wstr required_version_name = ConvertWindowsVersionToString(required_version, false);
                    FatalError(L"%s cannot run on %s, it requires %s or later", application_name.c_str(), current_version_name.c_str(), required_version_name.c_str());
                    return;
                }
            }
        }

        // exe
        if(!GetWideStringFromJsonValue(json["exe"], exe_to_run)) {
            LOG_Error("No executable specified!?");
            FatalError(L"Invalid config detected (2)");
            return;
        }

        // launcher_arg
        if(!GetWideStringFromJsonValue(json["launcher_arg"], launcher_arg)) {
            LOG_Warn("No launcher arg...");
        }

        // install_path
        if(!GetWideStringFromJsonValue(json["install_path"], install_path)) {
            LOG_Error("Can't find pathname");
            FatalError(L"Invalid config detected (3)");
            return;
        }
        install_path = MakePathCanonical(ExpandEnvStrings(install_path));
        LOG_Info("Folder: %s", install_path.c_str());

        // working_dir
        if(!GetWideStringFromJsonValue(json["working_dir"], working_dir)) {
            LOG_Warn("working_dir not specified");
        }
        working_dir = PathJoin(install_path, working_dir);
        LOG_Info("Working folder is %s", working_dir.c_str());

        int temp_max_downloads = default_max_downloads;
        if(!GetIntFromJsonValue(json["max_downloads"], temp_max_downloads) || temp_max_downloads < 1 || temp_max_downloads > max_max_downloads) {
            LOG_Error("max_downloads missing or bad, setting it to %d", default_max_downloads);
            temp_max_downloads = default_max_downloads;
        }
        max_downloads = temp_max_downloads;

        // check dlls
        auto dlls = json["check_dlls"];
        uint dll_count = dlls.getArrayLength();
        for(uint i = 0; i < dll_count; ++i) {
            auto dll = dlls[i];
            if(!dll.IsValid()) {
                LOG_Error("Bad json entry in check_dlls array");
                FatalError(L"Error downloading your application (4.5}");
                return;
            }
            wstr name;
            wstr url;
            if(!GetWideStringFromJsonValue(dll["name"], name)) {
                LOG_Error("Bad json entry in check_dlls array (invalid name)");
                FatalError(L"Error downloading your application (4.5.1}");
                return;
            }
            if(!GetWideStringFromJsonValue(dll["url"], url)) {
                LOG_Error("Bad json entry in check_dlls array (invalid url)");
                FatalError(L"Error downloading your application (4.5.2}");
                return;
            }
            // check if the dll can be loaded
            HMODULE h = LoadLibraryExW(name, null, LOAD_LIBRARY_AS_DATAFILE);
            if(h != null) {
                FreeLibrary(h);
            } else {
                if(MessageBoxW(main_window, $(L"%s not found, launch a webpage to download it?\n\n", name.c_str()), application_name, MB_YESNO) == IDYES) {
                    LaunchURL(url);
                } else {
                    LOG_Error("They don't wanna download %s", name.c_str());
                }
                return;
            }
        }

        // create the folders
        auto folders = json["folders"];
        int folder_count = (int)folders.getArrayLength();
        if(folder_count != 0) {
            for(int i = 0; i < folder_count; ++i) {
                wstr relative_folder_path;
                if(GetWideStringFromJsonValue(folders[i], relative_folder_path)) {
                    wstr folder_path = PathJoin(install_path, relative_folder_path);
                    LOG_Verbose("Creating folder: %s", folder_path.c_str());
                    if(!SUCCEEDED(CreateFolder(folder_path))) {
                        FatalError(L"Can't create folder %s\n%s", folder_path.c_str(), WindowsError(L"CreateFolder").c_str());
                        return;
                    }
                }
            }
        }

        // now get the files
        auto files = json["contents"];
        file_count = (int)files.getArrayLength();
        if(file_count == 0) {
            LOG_Warn("No files in folder %s", install_path.c_str());
            FatalError(L"Error downloading your application (5)");
            return;
        }

        // array of downloads
        downloads = new Download[file_count];
        defer(delete[] downloads);
        int download_count = 0;

        for(int i = 0; i < file_count; ++i) {
            JsonValue file_or_folder = files[i];
            if(file_or_folder.getType() == JsonType::Object) {
                Download &dnld = downloads[download_count];
                if(!dnld.InitFromJsonValue(files[i], install_path, version)) {
                    FatalError(L"Error downloading your application (6)");
                    return;
                }
                if(dnld.needs_downloading) {
                    total_download_size += (size_t)dnld.size;
                    download_count += 1;
                }
            }
        }

        LOG_Info("Total download is %lld bytes (%5.2fMB) (%d files out of %d)", total_download_size, total_download_size / 1048576.0, download_count, file_count);

        if(total_download_size > 0) {

            PostMessage(progress_bar, PBM_SETRANGE32, 0, (LPARAM)total_download_size);    // download limited to 4GB for now...

            // array of max_downloads handles for waiting for downloads to complete (+1 for cancel event)
            HANDLE *handles = new HANDLE[max_downloads + 1];
            defer(delete[] handles);

            int handle_count = 0;
            handles[handle_count++] = cancel_event;

            // the next one to download
            int next_file = 0;

            // total that got downloaded, just for logging
            int downloaded = 0;

            // loop until they're all downloaded
            while(true) {

                // need to kick off more?
                while(next_file < file_count) {
                    Download &r = downloads[next_file++];
                    if(r.needs_downloading) {
                        if(r.size == 0) {
                            LOG_Verbose("Empty file %s", r.filename.c_str());
                            if(!SUCCEEDED(CreateEmptyFile(r.filename))) {
                                LOG_Error("Error creating %s\n%s", r.filename.c_str(), WindowsError(L"CreateEmptyFile").c_str());
                                FatalError(L"Error downloading your application (6.5)");
                                return;
                            }
                            continue;
                        }
                        if(!r.Start()) {
                            return;
                        }
                        LOG_Info("Downloading %s", r.Url().c_str());
                        downloaded += 1;
                        handles[handle_count++] = r.ThreadHandle();
                        if(handle_count == (max_downloads + 1)) {
                            break;
                        }
                    }
                }

                // any left to do? if handle_count == 1 it means only the cancel event remains
                if(handle_count == 1) {
                    break;
                }

                // wait for one to complete (or the cancel event in slot 0)
                DWORD woken = WaitForMultipleObjects((DWORD)handle_count, handles, false, INFINITE);
                if(woken == WAIT_FAILED) {
                    LOG_Error("Error waiting for download?");
                    FatalError(L"Error downloading your application (7)");
                    return;
                }
                // cancelled?
                if(woken == WAIT_OBJECT_0) {
                    LOG_Info("Cancelled!");
                    Quit();
                    return;
                }

                // shift remaining wait handles down
                for(int i = woken; i < handle_count; ++i) {
                    handles[i] = handles[i + 1];
                }
                handle_count -= 1;
            }
            // downloads complete
            LOG_Info("%d files downloaded", downloaded);
        }

        // run the exe

        wstr current_dir = PathJoin(install_path, working_dir);
        wstr executable = PathJoin(current_dir, exe_to_run);

        wstr parameters;
        if(!launcher_arg.empty()) {
            parameters = ReplaceStr(launcher_arg, wstr(L"${LAUNCHER_PATH}"), $(L"\"%s\"", GetCurrentExeW()));
        }
        if(!SetCurrentDirectoryW(current_dir)) {
            FatalError(L"Can't set current directory to %s\n%s", current_dir.c_str(), WindowsError(L"SetCurrentDirectory").c_str());
            return;
        }
        if(LaunchExecutable(executable, current_dir, parameters)) {
            CloseMainWindow();
        } else {
            FatalError(L"Can't launch %s\n\n%s", application_name.c_str(), WindowsError(L"CreateProcess").c_str());
        }
    }

    // on successfully downloading the manifest
    void OnSuccess() override
    {
        // do the downloads
        DownloadFiles();    // if this goes well, it will close the main window
    }

    // manifest download failed
    void OnFailure() override
    {
        LOG_Error("%s\n", error_message.c_str());
        FatalError(L"Can't launch %s, there was a problem connecting to the server", application_name.c_str());
    }
};

//////////////////////////////////////////////////////////////////////

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
    // install log goes in
    wstr log_filename = ExpandEnvStrings(L"%temp%\\gerbil_installer.log");
    FILE *log_file = null;
    _wfopen_s(&log_file, log_filename, L"w");
    Log_SetOutputFiles(log_file, log_file);
    scoped[&]
    {
        if(log_file != null) {
            fclose(log_file);
        }
    };

    LOG_Level(Verbose);
    LOG_Info("WinMain");

    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    InitCommonControls();
    CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
    InitializeCriticalSection(&quit_critical_section);

    hInst = hInstance;

    wstr this_exe = GetCurrentExeW();

    // is this a gerbil upgrade?
    if(lpCmdLine != null && lpCmdLine[0] != 0) {
        // might be, get argc, argv
        int argc;
        wchar **argv = CommandLineToArgvW(lpCmdLine, &argc);
        wchar const *upgrade_arg = L"--upgrade-gerbil";
        size_t upgrade_arg_len = wcslen(upgrade_arg);
        // check if command line is --upgrade-gerbil "path_to_old_gerbil"
        if(argc == 2 && wcsncmp(argv[0], upgrade_arg, upgrade_arg_len) == 0) {
            // we're upgrading, load myself as a file
            MemoryBuffer<byte> exe_contents;
            if(!SUCCEEDED(LoadFile(this_exe, exe_contents))) {
                LOG_Error("Can't load %s %s: %s", this_exe.c_str(), WindowsError(L"LoadFile").c_str());
            } else {
                // try to overwrite the progenitor 10 times, over the course of about 1 second
                wstr new_exe(argv[1]);
                for(int attempts = 0; attempts < 10; ++attempts) {
                    Sleep(100);
                    if(!SUCCEEDED(SaveFile(new_exe, exe_contents.data, exe_contents.size))) {
                        LOG_Error("Can't save %s: %s", new_exe.c_str(), WindowsError(L"SaveFile").c_str());
                        continue;
                    }
                }
            }
            // failed to overwrite old launcher for whatever reason, now what?
            // tough shit I guess, if they run it again, it will end up back here eventually
        }
    }

    // we might have copied ourselves over an old gerbil launcher, no biggie
    // in any case, continue on with business as usual

    application_name = LoadResourceString(IDS_APP_NAME);

    // create some gdi objects
    static_background_brush = CreateSolidBrush(background_color);
    defer(DeleteObject(static_background_brush));

    HDC screen_dc = GetDC(null);
    int dpi_x = GetDeviceCaps(screen_dc, LOGPIXELSX);
    int dpi_y = GetDeviceCaps(screen_dc, LOGPIXELSX);
    ReleaseDC(null, screen_dc);
    static_logfont.lfHeight = static_logfont.lfHeight * dpi_y / 96;

    text_margin_x = text_margin_x * dpi_x / 96;
    text_margin_y = text_margin_y * dpi_y / 96;

    static_font = CreateFontIndirect(&static_logfont);
    defer(DeleteObject(static_font));

    // register window class
    wstr window_class(L"GERBIL");
    WNDCLASSEXW wcex = { 0 };
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_GERBIL));
    wcex.hCursor = LoadCursor(null, IDC_ARROW);
    wcex.hbrBackground = static_background_brush;
    wcex.lpszClassName = window_class;
    wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_GERBIL));
    if(!RegisterClassExW(&wcex)) {
        WindowsErrorMessage(L"Can't register class");
        return 1;
    }

    // create main window
    DWORD constexpr window_style = WS_CAPTION | WS_SYSMENU | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_THICKFRAME;
    RECT rc = progress_rect;
    ScaleRectByCurrentDPI(rc);
    AdjustWindowRect(&rc, window_style, false);
    CenterRectInDefaultMonitor(rc);
    int width = rc.right - rc.left;
    int height = rc.bottom - rc.top;
    main_window = CreateWindowExW(WS_EX_COMPOSITED, window_class, application_name, window_style, rc.left, rc.top, width, height, null, null, hInstance, null);
    if(!main_window) {
        WindowsErrorMessage(L"Can't create a window!?");
        return 1;
    }
    RECT client_rect;
    GetClientRect(main_window, &client_rect);

    // create message_text window
    constexpr uint32 message_window_flags = WS_CHILD | SS_CENTER;
    message_text = CreateWindowExW(WS_EX_COMPOSITED, L"STATIC", null, message_window_flags, text_margin_x, text_margin_y, client_rect.right - text_margin_x * 2,
                                   client_rect.bottom - text_margin_y * 2, main_window, null, hInst, 0);
    if(message_text == null) {
        WindowsErrorMessage(L"Can't create message text control!?");
        return 1;
    }
    SendMessage(message_text, WM_SETFONT, (WPARAM)static_font, 0);

    // create progress_bar window
    progress_bar = CreateWindowExW(WS_EX_COMPOSITED, PROGRESS_CLASSW, null, WS_CHILD | PBS_SMOOTH, text_margin_x, text_margin_y, client_rect.right - text_margin_x * 2,
                                   client_rect.bottom - text_margin_y * 2, main_window, null, hInstance, null);
    if(progress_bar == null) {
        WindowsErrorMessage(L"Can't create progress bar!?");
        return 1;
    }
    SendMessage(progress_bar, PBM_SETBARCOLOR, 0, foreground_color);
    SendMessage(progress_bar, PBM_SETBKCOLOR, 0, background_color);

    // create button
    RECT button_rect{ 0, 0, 100, 25 };
    ScaleRectByCurrentDPI(button_rect);
    button = CreateWindowExW(WS_EX_COMPOSITED, L"BUTTON", null, WS_CHILD | BS_FLAT, 0, 0, button_rect.right, button_rect.bottom, main_window, (HMENU)BUTTON_ID, hInstance, null);
    if(button == null) {
        WindowsErrorMessage(L"Can't create button window!?");
        return 1;
    }
    SendMessage(button, WM_SETFONT, (LPARAM)GetStockObject(DEFAULT_GUI_FONT), true);

    window_created = true;

    // create cancel and quit events
    cancel_event = CreateEvent(null, true, false, null);
    if(cancel_event == INVALID_HANDLE_VALUE) {
        WindowsErrorMessage(L"Can't create cancel_event");
        return 2;
    }

    quit_event = CreateEvent(null, true, false, null);
    if(quit_event == INVALID_HANDLE_VALUE) {
        WindowsErrorMessage(L"Can't create quit_event");
        return 3;
    }

    // kick off the manifest download
    url_base.assign(LoadResourceString(1));
    wstr manifest_url = UrlJoin(url_base, LoadResourceString(2));
    ManifestDownloader manifest_downloader;
    manifest_downloader.Start(manifest_url);

    // then just process messages
    MSG msg;
    while(GetMessage(&msg, null, 0, 0)) {
        DispatchMessage(&msg);
    }

    if(!quit_url.empty()) {
        LaunchURL(quit_url);
    }

    return (int)msg.wParam;
}