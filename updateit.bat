rem build a manifest which has an upgraded gerbil launcher in it

release\gerbil^
 --interim-folder "c:\gerbil_temp2"^
 --clean-interim-folder^
 --min-version 6.1.1^
 --launcher-filename "c:\gerbil_temp2\launcher.exe"^
 --manifest-name "manifest.gerbil"^
 --url-base "https://skilbeck.com/gerbil/2/"^
 --source-folder "c:\projects\gerbil\release"^
 --install-path "%%localappdata%%\Polystream\client\update"^
 --exe-file "launcher.exe"^
 --filespecs "launcher.exe"^
 --application-name "Updater"^
 --launcher-arg "--upgrade-gerbil ${LAUNCHER_PATH}" >c:\gerbil_temp2\manifest.gerbil
