//////////////////////////////////////////////////////////////////////
// TODO (chs): namespace this stuff

#pragma once

//////////////////////////////////////////////////////////////////////

template <typename T> void Log(int level, T const *context, T const *msg, ...);
void Log_SetOutputFiles(FILE *log_file, FILE *error_log_file);

//////////////////////////////////////////////////////////////////////

enum LogLevel
{
    max_log_level = 4,
    Verbose = 4,
    Debug = 3,
    Info = 2,
    Warn = 1,
    Error = 0,
    min_log_level = 0
};

//////////////////////////////////////////////////////////////////////

#if !defined(DISABLE_LOGGING)

void Log_SetLevel(int level);

#define LOG_Level(l) Log_SetLevel(l)

#define LOG_Context(c)                                     \
    static constexpr wchar const *__log_context = TEXT(c); \
    static constexpr char const *__log_contextA = c;       \
    static constexpr wchar const *__log_contextW = L##c

#define LOG_Print(level, msg, ...) Log(level, __log_context, msg, __VA_ARGS__)
#define LOG_PrintA(level, msg, ...) Log(level, __log_contextA, msg, __VA_ARGS__)
#define LOG_PrintW(level, msg, ...) Log(level, __log_contextW, msg, __VA_ARGS__)

#define LOG_VerboseA(msg, ...) Log(Verbose, __log_contextA, msg, __VA_ARGS__)
#define LOG_DebugA(msg, ...) Log(Debug, __log_contextA, msg, __VA_ARGS__)
#define LOG_InfoA(msg, ...) Log(Info, __log_contextA, msg, __VA_ARGS__)
#define LOG_WarnA(msg, ...) Log(Warn, __log_contextA, msg, __VA_ARGS__)
#define LOG_ErrorA(msg, ...) Log(Error, __log_contextA, msg, __VA_ARGS__)

#define LOG_VerboseW(msg, ...) Log(Verbose, __log_contextW, msg, __VA_ARGS__)
#define LOG_DebugW(msg, ...) Log(Debug, __log_contextW, msg, __VA_ARGS__)
#define LOG_InfoW(msg, ...) Log(Info, __log_contextW, msg, __VA_ARGS__)
#define LOG_WarnW(msg, ...) Log(Warn, __log_contextW, msg, __VA_ARGS__)
#define LOG_ErrorW(msg, ...) Log(Error, __log_contextW, msg, __VA_ARGS__)

#define LOG_Verbose(msg, ...) Log(Verbose, __log_context, TEXT(msg), __VA_ARGS__)
#define LOG_Debug(msg, ...) Log(Debug, __log_context, TEXT(msg), __VA_ARGS__)
#define LOG_Info(msg, ...) Log(Info, __log_context, TEXT(msg), __VA_ARGS__)
#define LOG_Warn(msg, ...) Log(Warn, __log_context, TEXT(msg), __VA_ARGS__)
#define LOG_Error(msg, ...) Log(Error, __log_context, TEXT(msg), __VA_ARGS__)

//////////////////////////////////////////////////////////////////////

template <typename T> struct _log
{
    static void DebugWrite(T const *s) = delete;
    static void FileWrite(FILE *f, T const *s) = delete;
    static constexpr T const *log_format_string = null;
    static constexpr char const *LOG_Names[] = {};
};

//////////////////////////////////////////////////////////////////////

template <> struct _log<char>
{
    static void DebugWrite(char const *s)
    {
        OutputDebugStringA(s);
    }
    static void FileWrite(FILE *f, char const *s)
    {
        fputs(s, f);
    }
    static constexpr char const *log_format_string = "%s %-20.20s %s\n";

    // clang-format off
	static constexpr char const *LOG_Names[] = {
		"Error  ",
		"Warn   ",
		"Info   ",
		"Debug  ",
		"Verbose"
	};
    // clang-format on
};

//////////////////////////////////////////////////////////////////////

template <> struct _log<wchar>
{
    static void DebugWrite(wchar const *s)
    {
        OutputDebugStringW(s);
    }
    static void FileWrite(FILE *f, wchar const *s)
    {
        fputws(s, f);
    }
    static constexpr wchar const *log_format_string = L"%s %-20.20s %s\n";

    // clang-format off
	static constexpr wchar const *LOG_Names[] = {
		L"Error  ",
		L"Warn   ",
		L"Info   ",
		L"Debug  ",
		L"Verbose"
	};
    // clang-format on
};

//////////////////////////////////////////////////////////////////////

template <typename T> void Log(int level, T const *context, T const *msg, ...)
{
    using L = _log<T>;
    extern int log_level;
    extern FILE *log_file;
    extern FILE *error_log_file;

    if(level > log_level) {
        return;
    }
    if(level < min_log_level) {
        level = (int)min_log_level;
    }
    if(level > max_log_level) {
        level = (int)max_log_level;
    }
    va_list v;
    va_start(v, msg);
    str_base<T> s = $(L::log_format_string, L::LOG_Names[level], context, str_base<T>::format(msg, v).c_str());
    L::DebugWrite(s.c_str());
    if(level == Error && error_log_file != null) {
        L::FileWrite(error_log_file, s);
    } else if(log_file != null) {
        L::FileWrite(log_file, s);
    }
}

//////////////////////////////////////////////////////////////////////

#else

#define LOG_SetLevel(l) \
    if(false) {         \
    } else

#define LOG_Level(l) \
    if(false) {      \
    } else

#define LOG_Context(c)

#define LOG_Print(level, msg, ...) \
    if(false) {                    \
    } else
#define LOG_PrintA(level, msg, ...) \
    if(false) {                     \
    } else
#define LOG_PrintW(level, msg, ...) \
    if(false) {                     \
    } else

#define LOG_VerboseA(msg, ...) \
    if(false) {                \
    } else
#define LOG_DebugA(msg, ...) \
    if(false) {              \
    } else
#define LOG_InfoA(msg, ...) \
    if(false) {             \
    } else
#define LOG_WarnA(msg, ...) \
    if(false) {             \
    } else
#define LOG_ErrorA(msg, ...) \
    if(false) {              \
    } else

#define LOG_VerboseW(msg, ...) \
    if(false) {                \
    } else
#define LOG_DebugW(msg, ...) \
    if(false) {              \
    } else
#define LOG_InfoW(msg, ...) \
    if(false) {             \
    } else
#define LOG_WarnW(msg, ...) \
    if(false) {             \
    } else
#define LOG_ErrorW(msg, ...) \
    if(false) {              \
    } else

#define LOG_Verbose(msg, ...) \
    if(false) {               \
    } else
#define LOG_Debug(msg, ...) \
    if(false) {             \
    } else
#define LOG_Info(msg, ...) \
    if(false) {            \
    } else
#define LOG_Warn(msg, ...) \
    if(false) {            \
    } else
#define LOG_Error(msg, ...) \
    if(false) {             \
    } else

//////////////////////////////////////////////////////////////////////

#endif
