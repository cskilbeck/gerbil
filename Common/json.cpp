#include <string.h>
#include <stdlib.h>
#include "json.h"

static char const *eoj;

bool eof(char const *ch)
{
    return *ch == 0 || ch >= eoj;
}

int strcompare(char const *ch, char const *s, int required)
{
    intptr_t left = (intptr_t)(eoj - ch);
    if(left < required) {
        required = (int)left;
    }
    return strncmp(ch, s, required);
}

bool skip_digits(char const *&ch)
{
    while(true) {
        if(eof(ch)) {
            return false;
        }
        if(*ch < '0' || *ch > '9') {
            return true;
        }
        ch += 1;
    }
}

bool skip_minus(char const *&ch)
{
    if(eof(ch)) {
        return false;
    }
    if(*ch == '-') {
        ch++;
    }
    return true;
}

bool skip_whitespace(char const *&ch)
{
    while(true) {
        if(eof(ch)) {
            return false;
        }
        if(*ch != ' ' && *ch != '\t' && *ch != '\r' && *ch != '\n') {
            return true;
        }
        ++ch;
    }
}

bool skip_comment(char const *&ch)
{
    while(true) {
        if(eof(ch)) {
            return false;
        }
        if(*ch == '\n') {
            ++ch;
            return true;
        }
        ++ch;
    }
}

JsonToken eof_token(char const *ch)
{
    return { ch, 0, JsonTokenType::Eof };
}

JsonToken parseToken(const char *&ch)
{
    if(eof(ch)) {
        return eof_token(ch);
    }

    if(!skip_whitespace(ch)) {
        return eof_token(ch);
    }

    if(*ch == '/') {
        ch += 2;
        if(!skip_comment(ch)) {
            return eof_token(ch);
        }
    }

    JsonToken token{ ch, 0, JsonTokenType::Unknown };

    auto start = ch;

    switch(*ch) {
    case '\0':
        token.type = JsonTokenType::Eof;
        ch++;
        break;
    case '{':
        token.type = JsonTokenType::Object_Start;
        ch++;
        break;
    case '}':
        token.type = JsonTokenType::Object_End;
        ch++;
        break;
    case ':':
        token.type = JsonTokenType::KeyValue_Separator;
        ch++;
        break;
    case ',':
        token.type = JsonTokenType::Array_Separator;
        ch++;
        break;
    case '[':
        token.type = JsonTokenType::Array_Start;
        ch++;
        break;
    case ']':
        token.type = JsonTokenType::Array_End;
        ch++;
        break;
    case '"':
    case '\'':
        do {
            while(!eof(++ch) && *ch != *start && *ch != '\\') {
            }
            if(eof(ch) || *ch == '\\' && eof(++ch)) {
                token.type = JsonTokenType::Unterminated_String;
                return token;
            }
        } while(*ch != *start);

        token.token = start + 1;
        token.length = ch - token.token;
        token.type = JsonTokenType::String;
        ch++;
        break;
    case 't':
        if(strcompare(ch, "true", 4) == 0) {
            token.type = JsonTokenType::Boolean_True;
            token.length = 4;
            ch += 4;
        }
        break;
    case 'f':
        if(strcompare(ch, "false", 5) == 0) {
            token.type = JsonTokenType::Boolean_False;
            token.length = 5;
            ch += 5;
        }
        break;
    case 'n':
        if(strcompare(ch, "null", 4) == 0) {
            token.type = JsonTokenType::Null;
            token.length = 4;
            ch += 4;
        }
        break;
    default:
        token.type = JsonTokenType::Unknown;
        if(!skip_minus(ch)) {
            break;
        }
        auto numStart = ch;
        if(!skip_digits(ch)) {
            break;
        }
        if(*ch == '.') {
            ch++;
            if(!skip_digits(ch)) {
                break;
            }
        }
        token.type = JsonTokenType::Number;
        if(ch - numStart == 0) break;
        if(*ch == 'e' || *ch == 'E') {
            ch++;
            token.type = JsonTokenType::Unknown;
            if(!skip_minus(ch)) {
                break;
            }
            if(!skip_digits(ch)) {
                break;
            }
        }
        token.type = JsonTokenType::Number;
        token.length = ch - start;
        break;
    }
    return token;
}

JsonValue JsonValue::operator[](const char *key)
{
    if(m_token != nullptr && m_token->type == JsonTokenType::Object_Start) {
        JsonValue v = getObjectValue(key, -1);
        return v;
    }
    return {};
}

JsonValue JsonValue::operator[](size_t index)
{
    if(m_token->type == JsonTokenType::Array_Start)
        return getIndexValue(index);
    else if(m_token->type == JsonTokenType::Object_Start)
        return getObjectValue(nullptr, static_cast<int>(index));

    return {};
}

size_t JsonValue::getArrayLength()
{
    if(!IsValid()) {
        return 0;
    }

    if(m_token->type == JsonTokenType::Array_Start) {
        if(m_arrLen == -1) {
            m_arrLen = static_cast<int>(calculateLength());
        }
        return m_arrLen;
    }

    return 0;
}

int JsonValue::getInt()
{
    return atoi(m_token->token);
}

bool JsonValue::getBool() const
{
    return (m_token->token[0] == 't') || (m_token->token[0] == 'T');
}

unsigned long long JsonValue::getUll()
{
    return strtoull(m_token->token, nullptr, 10);
}

int64_t JsonValue::getInt64() const
{
    return strtoll(m_token->token, nullptr, 10);
}

float JsonValue::getFloat()
{
    return strtof(m_token->token, nullptr);
}

uint16_t JsonValue::getUShort()
{
    return (uint16_t)getInt();
}

size_t JsonValue::getTokenLength() const
{
    return m_token->length;
}

const char *JsonValue::getString(char *buffer, size_t size) const
{
    if(m_token != nullptr) {
        const char *str;
        rsize_t len;

        if(m_token->type == JsonTokenType::Object_Start) {
            str = "{Object}";
            len = 8;
        } else if(m_token->type == JsonTokenType::Array_Start) {
            str = "{Array}";
            len = 7;
        } else {
            str = m_token->token;
            len = m_token->length;
        }

        strncpy_s(buffer, size, str, len);
        buffer[len] = '\0';
    } else {
        // just terminate
        buffer[0] = '\0';
    }
    return buffer;
}

JsonValue JsonValue::parse(const JsonToken *token)
{
    if(token->type != JsonTokenType::Object_Start) return {};

    return JsonValue{ token };
}

size_t JsonValue::calculateLength()
{
    if(m_token == nullptr) return 0;

    if(m_token->type != JsonTokenType::Array_Start) return 0;

    auto token = m_token;

    size_t i = 0;

    ++token;

    while(token->type != JsonTokenType::Array_End) {
        if(token->type == JsonTokenType::Array_Start)
            skipTo(token, JsonTokenType::Array_End, JsonTokenType::Array_Start);
        else if(token->type == JsonTokenType::Object_Start)
            skipTo(token, JsonTokenType::Object_End, JsonTokenType::Object_Start);

        ++token;
        ++i;

        if(token->type == JsonTokenType::Array_Separator) ++token;
    }

    return i;
}

JsonValue JsonValue::getIndexValue(size_t index)
{
    if(m_token == nullptr) return {};

    if(m_token->type != JsonTokenType::Array_Start) return {};

    auto token = m_token;

    size_t i = 0;

    do {
        ++token;

        if(i == index) {
            if(!isValue(token)) return {};

            return JsonValue{ token };
        }

        if(token->type == JsonTokenType::Array_Start)
            skipTo(token, JsonTokenType::Array_End, JsonTokenType::Array_Start);
        else if(token->type == JsonTokenType::Object_Start)
            skipTo(token, JsonTokenType::Object_End, JsonTokenType::Object_Start);

        ++i;
    } while((++token)->type == JsonTokenType::Array_Separator);

    return {};
}

JsonValue JsonValue::getObjectValue(const char *key, int index)
{
    if(m_token == nullptr) return {};

    auto token = m_token;

    if(token->type != JsonTokenType::Object_Start) return {};

    size_t i = 0;

    while((++token)->type != JsonTokenType::Object_End && token->type != JsonTokenType::Eof) {
        if(token->type != JsonTokenType::String) return {};

        auto keyToken = *token;

        if((++token)->type != JsonTokenType::KeyValue_Separator) return {};

        auto value = (++token);

        if(!isValue(token)) return {};

        if(key != nullptr) {
            if(strncmp(key, keyToken.token, keyToken.length) == 0) return JsonValue{ value };
        } else if(index > -1) {
            if(i == size_t(index)) return JsonValue{ value };
        }

        if(token->type == JsonTokenType::Array_Start)
            skipTo(token, JsonTokenType::Array_End, JsonTokenType::Array_Start);
        else if(token->type == JsonTokenType::Object_Start)
            skipTo(token, JsonTokenType::Object_End, JsonTokenType::Object_Start);

        if((token + 1)->type == JsonTokenType::Array_Separator) ++token;

        ++i;
    }

    return {};
}

void JsonValue::skipTo(const JsonToken *&token, JsonTokenType closingType, JsonTokenType openingType)
{
    int level = 1;

    do {
        ++token;

        if(token->type == openingType)
            ++level;
        else if(token->type == closingType)
            --level;

    } while(token->type != closingType || level > 0);
}

void append_token(JsonToken *tokens, JsonToken const &token, size_t &tokenCount, size_t const maxSize)
{
    if(tokenCount < maxSize) {
        tokens[tokenCount] = token;
    }
    tokenCount += 1;
}

errno_t jsonTokenize(const char *json, size_t size, JsonToken *tokens, size_t maxSize, size_t &tokenCount)
{
    char const *ptr = json;
    JsonToken token;
    tokenCount = 0;
    if(json == nullptr) {
        return -2;
    }
    eoj = json + size;
    while(true) {
        token = parseToken(ptr);
        if(eof(ptr) || token.type == JsonTokenType::Eof || token.type == JsonTokenType::Unknown) {
            break;
        }
        append_token(tokens, token, tokenCount, maxSize);
    }
    if(token.type != JsonTokenType::Eof) {
        append_token(tokens, eof_token(ptr), tokenCount, maxSize);
    }
    if(ptr == eoj) {
        return 0;
    }
    return -1;
}
