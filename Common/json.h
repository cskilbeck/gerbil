#pragma once

#include <stdint.h>

enum class JsonTokenType
{
    Object_Start,
    Object_End,
    Array_Start,
    Array_End,
    KeyValue_Separator,
    Array_Separator,
    String,
    Number,
    Boolean_True,
    Boolean_False,
    Null,
    Eof,
    Unknown,
    Unterminated_String
};

enum class JsonType
{
    Object,
    Array,
    String,
    Number,
    Boolean,
    Null
};

struct JsonToken
{
    const char *token;
    size_t length;
    JsonTokenType type;
};

class JsonValue
{
    const JsonToken *m_token;
    int m_arrLen = -1;

public:
    JsonValue() : m_token(nullptr)
    {
    }

    explicit JsonValue(const JsonToken *token) : m_token(token)
    {
    }

    bool IsValid() const
    {
        return m_token != nullptr;
    }
    bool IsNull() const
    {
        return (m_token == nullptr) || (m_token->type == JsonTokenType::Null);
    }

    static JsonValue JsonValue::parse(const JsonToken *token);

    JsonValue operator[](const char *key);
    JsonValue operator[](size_t index);
    size_t getTokenLength() const;
    size_t getArrayLength();
    const char *getString(char *buffer, size_t size) const;
    int getInt();
    float getFloat();
    uint16_t getUShort();
    unsigned long long getUll();
    int64_t getInt64() const;
    bool getBool() const;

    JsonType getType() const
    {
        switch(m_token->type) {
        case JsonTokenType::Array_Start:
            return JsonType::Array;
        case JsonTokenType::Object_Start:
            return JsonType::Object;
        case JsonTokenType::String:
            return JsonType::String;
        case JsonTokenType::Number:
            return JsonType::Number;
        case JsonTokenType::Boolean_False:
        case JsonTokenType::Boolean_True:
            return JsonType::Boolean;
        }

        return JsonType::Null;
    }

private:
    size_t calculateLength();
    JsonValue getIndexValue(size_t index);
    JsonValue getObjectValue(const char *key, int index);
    void skipTo(const JsonToken *&token, JsonTokenType closingType, JsonTokenType openingType);

    bool isValue(const JsonToken *token)
    {
        return token->type == JsonTokenType::String || token->type == JsonTokenType::Number || token->type == JsonTokenType::Null || token->type == JsonTokenType::Boolean_True ||
               token->type == JsonTokenType::Boolean_False || token->type == JsonTokenType::Object_Start || token->type == JsonTokenType::Array_Start;
    }
};

errno_t jsonTokenize(const char *json, size_t size, JsonToken *tokens, size_t maxSize, size_t &tokenCount);
JsonToken parseToken(const char *&ch);
