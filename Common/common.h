//////////////////////////////////////////////////////////////////////

#pragma once

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN

#if !defined(UNICODE)
#error "Build it with unicode"
#endif

#define _WIN32_WINNT _WIN32_WINNT_WINXP
#include <SDKDDKVer.h>

#include <windows.h>
#include <winuser.h>
#include <winhttp.h>
#include <Shlwapi.h>
#include <Shlobj.h>
#include <mmsystem.h>

//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <assert.h>
#include <inttypes.h>
#include <cstddef>

//////////////////////////////////////////////////////////////////////

using byte = unsigned char;
constexpr nullptr_t null = nullptr;
using wchar = wchar_t;
using tchar = TCHAR;
using uint = unsigned int;
using uint32 = uint32_t;
using uint64 = uint64_t;

//////////////////////////////////////////////////////////////////////

#include "Debug.h"
#include "Log.h"
#include "Defer.h"
#include "MemoryBuffer.h"
#include "Checksum.h"
#include "File.h"

constexpr uint default_max_downloads = 8;
constexpr uint max_max_downloads = 32;
