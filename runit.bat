@echo off

REM interim_folder is where the launcher, manifest and files for upload will go

set interim_folder=c:\gerbil_temp

REM exe_file is the file which will be run after the app is successfully downloaded

set exe_file=psclient_native.exe

REM file_specs is what you want to be included, you can put multiple ones separated by semicolons (eg *.exe;*.dll;*.ini)

set file_specs=*.*

REM app_name is a user friendly name of the application being installed

set app_name=Euro Fishing

REM source_folder is where the files to be scanned live

set source_folder=d:\projects\polystream\Client\Build\Production Static\x64

REM url_base is where the launcher will download the manifest and files from

set url_base=https://skilbeck.com/gerbil/

REM launcher_name is what the launcher executable will be called

set launcher_name=EuroFishing_InstantLaunch.exe

REM icon_file will be embedded in the launcher executable (must be 32x32, 8 bit, 1 bit mask)

set icon_file=d:\projects\gerbil\eurofishing.ico

echo Building launcher for %app_name%

mkdir %interim_folder% >nul 2>nul

release\gerbil --interim-folder "%interim_folder%" --clean-interim-folder --icon-file "%icon_file%" --min-version 6.1.1 --launcher-filename "%interim_folder%\%launcher_name%" --manifest-name "manifest.gerbil" --url-base "%url_base%" --source-folder "%source_folder%" --install-path "%%localappdata%%\Polystream\client\%app_name%" --exe-file "%exe_file%" --filespecs "%file_specs%" --application-name "%app_name%" --max-downloads 8 --launcher-arg "--launched-by ${LAUNCHER_PATH}" --libraries "dxgi.dll;https://support.microsoft.com/en-us/help/179113/how-to-install-the-latest-version-of-directx" >%interim_folder%\manifest.gerbil 

echo Done, please upload the contents of %interim_folder% to %url_base%
