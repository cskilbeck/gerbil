# Gerbil

## Aims

- Small initial download size
- Update application without user interaction
- Preemptively check for DLL dependencies (eg DirectX on Windows XP)

## Method

A small manifest file is downloaded which contains this information

- The required client files
- Where they should be installed
- The working directory for the application
- What executable to launch when the download is complete
- Dependent external DLLs to check for the existence of
- Miscellaneous other crap

### Pseudocode
```text
get manifest file
check windows version if specified
check dependent DLLs
for each file in 'contents'
    if file does not exist or checksum does not match
        download file to install_path\filename
launch executable in the correct folder
```
### Manifest file

The manifest file is a JSON format text file with the following top level members:

| Field                | type  | required  |description                                                                                                                    |
|----------------------|-------|-----------|-------------------------------------------------------------------------------------------------------------------------------|
| `application_name`   | string| mandatory |specifies the user friendly name of the application being installed                                                            |
| `install_path`       | string| mandatory |specifies where the install goes. Environment variables will be expanded on the client machine                                 |
| `version`            | string| mandatory |current version of the install (also the name of the folder on the server where the install files are located)                 |
| `max_downloads`      | int   | optional  |max number of concurrent downloads to allow when downloading the app (defaults to 8)                                           |
| `launcher_arg`       | string| optional  |argument which will be passed to the application at launch time. `${LAUNCHER_PATH}` will be replaced with installer path       |
| `working_dir`        | string| optional  |relative path to set as working directory when launching the exe. If it's null or missing, the `install_path` will be used     |
| `exe`                | string| mandatory |filename, relative to `install_path`, of the executable to launch                                                              |
| `check_dlls`         | string| mandatory |semicolon delimited list of strings, each pair of strings denotes a dll to check for the existence of and a url to launch if that dll can't be loaded|
| `min_version`        | string| optional  |Minimum windows version required                                                                                               |
| `max_version`        | string| optional  |Maximum windows version allowed                                                                                                |
| `client_only`        | bool  | optional  |If true, don't allow it to run on Windows Server                                                                               |
| `server_only`        | bool  | optional  |If true, don't allow it to run on Windows Client                                                                               |
| `folders`            | array | optional  |array of folders to create at install time                                                                                     |
| `contents`           | array | mandatory |array of `file` objects                                                                                                        |

The 'file' object has the following fields:

| Field         | type  | required  |description                                                                                                                    |
|---------------|-------|-----------|-------------------------------------------------------------------------------------------------------------------------------|
|`file`         |string |mandatory  |relative filename of the file to download                                                                                      |
|`size`         |number	|mandatory	|size in bytes of the file																										|
|`md5|sha256`   |string |mandatory  |hex string of the SHA256 or MD5 of the file                                                                                    |

### Example manifest file

```text
{
  "application_name":"My Application",
  "install_path":"%localappdata%\\Publisher\\client\\app\\3.1.0-unstable.1059",
  "max_downloads":8,
  "launcher_arg":"--launched-by ${LAUNCHER_PATH}",
  "exe":"myapp.exe",
  "version":1,
  "min_version": "6.1",
  "client_only": true,
  "check_dlls":[
    {
      "name":"d3d9.dll",
      "url":"https://support.microsoft.com/en-us/help/179113/how-to-install-the-latest-version-of-directx"
    },
    {
      "name":"dxgi.dll",
      "url":"https://support.microsoft.com/en-us/help/179113/how-to-install-the-latest-version-of-directx"
    }
  ],
  "folders":[
      "data"
  ],
  "contents":[
    {
      "file":"myapp.exe",
      "size":8105288,
      "hash":"6edeee90b36ab917bde2a0af89d08ec2"
    },
    {
      "file":"myapp.dll",
      "size":3127808,
      "hash":"ce2e5c3a31e65a35a59e357dfa80000c"
    },
    {
      "file":"data\\mydata.bin",
      "size":14774272,
      "hash":"8ed4916317c91660b7fc23ea5d6ff3ce"
    }
  ]
}
```

### Manifest Tool

To generate a manifest, use the gerbil command line tool. The manifest will be output to the interim-folder or the current folder if an interim-folder is not specified.

The manifest tool will return a value as follows:

|Error   |Enum|Description
|--------|-------|---|
|0       |SUCCESS|Completed successfully|
|1       |ERROR_USAGE|Command line args error, check stderr for details|
|2       |ERROR_UPDATING_LAUNCHER_RESOURCES|Couldn't update the launcher exe resource section|
|4       |ERROR_ICON_NOT_COMPATIBLE|Icon for embedding must be 2238 bytes long, 32x32x8bit, 1bit mask|
|5       |ERROR_LOADING_ICON_FILE|Error loading the icon file|
|6       |ERROR_SAVING_LAUNCHER_EXE|Error saving the new launcher executable|
|7       |ERROR_LOADING_EXE_RESOURCE|Error loading the embedded launcher exe resource|
|8       |ERROR_SCANNING_FOLDER|Error scanning the source folder|
|10      |ERROR_BAD_LIBRARIES_ARGUMENT|Libraries argument isn't an even number of semicolon delimited strings|
|11      |ERROR_WRITING_TO_MANIFEST_FILE|Couldn't write to manifest file|

The usage is:

`gerbil [options]`

#### Options:

|Option                             |Required |Description|
|-----------------------------------|---------|--------------------------------|
|`--help`                           |Optional |Print usage and exit.|
|`--source-folder` `path`           |Mandatory|Where the files to be installed are located|
|`--install-path` `path`            |Mandatory|Where the application will be installed on the target machine. Environment variables will be expanded at runtime|
|`--exe-file` `filename`            |Mandatory|The file which will be launched after a successfull installation|
|`--application-name` `string`      |Mandatory|User friendly application display name|
|`--launcher-filename` `filename`   |Mandatory|Where to save the downloader executable|
|`--url-base` `string`              |Mandatory|URL base of manifest and files location|
|`--version` `string/path`          |Mandatory|Version (actually, folder name where install files will be stored on the server)|
|`--min-version` `version`          |Optional |Min windows version supported (see below), default is any version|
|`--max-version` `version`          |Optional |Max windows version supported (see below), default is any version|
|`--working-folder` `path`          |Optional |Working folder to be set when launching the application (relative to install-path), defaults to install-path|
|`--checksum-type` `md5/sha256`     |Optional |Set checksum type, default is sha256 (md5 is deprecated)|
|`--manifest-name` `string`         |Optional |Name of manifest file on the server, default is manifest.gerbil|
|`--max-downloads` `integer(1..32)` |Optional |Max concurrent downloads to allow during install, default is 32|
|`--interim-folder` `path`          |Optional |If specified, all scanned files will be copied to this folder|
|`--clean-interim-folder`           |Optional |Flag. If specified, and interim-folder is specified, interim-folder will be cleaned before files are copied into it|
|`--filespecs speclist`             |Optional |Semicolon separated list of filespecs to include in the installation (eg "*.exe;*.dll"), default is *.*|
|`--launcher-arg` `argstring`       |Optional |Args that will be passed to the exe when it is run, you can use the special macro ${LAUNCHER_PATH} to denote the installer exe|
|`--libraries` `liblist`            |Optional |Semicolon separated list of string pairs where the 1st is the name of a DLL (eg d3d9.dll) and the 2nd is the url to launch if it's not there at install time|
|`--icon-file` `filename`           |Optional |Icon file to embed into the launcher (must be a 32x32, 8 bit palette, 1 bit alpha .ico file, which will be 2238 bytes long)|
|`--verbosity` `integer(0-4)`       |Optional |0 = errors, 1 = warnings, 2 = info, 3 = debug, 4 = verbose, default is 2 (info)|
|`--client-only`                    |Optional |Flag. If specified, application will run only on client versions of Windows|
|`--server-only`                    |Optional |Flag. If specified, application will run only on server versions of Windows|
|`--exclude-empty-folders`          |Optional |Flag. If specified, empty folders will not be included in the manifest|
|`--exclude-empty-files`            |Optional |Flag. If specified, empty files (0 bytes length) will not be included in the manifest|

#### Windows Versions
When specifying versions for min-version and max-version, the following values should be used:

|Version |Client        |Server                     |
|--------|--------------|---------------------------|
|10.0    |Windows 10    |Windows Server 2016        |
|6.3     |Windows 8.1   |Windows Server 2012 R2     |
|6.2     |Windows 8     |Windows Server 2012        |
|6.1     |Windows 7     |Windows Server 2008 R2     |
|6.0     |Windows Vista	|Windows Server 2008        |
|5.2     |              |Windows Server 2003 [R2]   |
|5.1     |Windows XP    |                           |
|5.0     |Windows 2000  |                           |

To specify a service pack, add a second point and a 3rd number.

Examples:

|`--min-version`|`--server-only`|`--client-only`|Result  |
|---------------|---------------|---------------|--------|
|`6.1`          |Not specified  |Not specified  |Windows 7 and later or Windows Server 2008 R2 and later|
|`6.0.1`        |Not specified  |Specified      |Windows Vista SP1 and later, not Windows Server anything|
|`6.3`          |Specified      |Not specified  |Windows Server 2012 R2 and later, not Windows Client anything|
|`6.2`          |Specified      |Specified      |Error, can't specify `--server-only` and `--client-only`|

### To upgrade the launcher
If you need to upgrade the launcher, create a manifest which contains just the new launcher (specify the new launcher as the exe-file) and set launcher-arg like `--upgrade-gerbil ${LAUNCHER_PATH}`. Set this is as your current manifest, so the old launcher will download
that manifest, download the new launcher and run it with `--upgrade-gerbil ${LAUNCHER_PATH}`, the new launcher will copy itself over the old launcher and continue.

The flow looks like:

##### Normal flow
- `User downloads Gerbil`
- `User runs Gerbil`
- `Gerbil downloads Manifest`
- `Gerbil downloads Client`
- `Gerbil launches Client`
- `Gerbil exits`
- `User uses Client`
- `Client exits`

##### Upgrade flow
- `User runs Gerbil`
- `Gerbil downloads Upgrade Manifest`
- `Gerbil downloads New Gerbil`
- `Gerbil launches New Gerbil --upgrade-gerbil {path to old gerbil}`
- `Gerbil exits`
- `New Gerbil overwrites Gerbil with New Gerbil`
- `New Gerbil downloads New Manifest`
- `New Gerbil downloads New Client`
- `New Gerbil launches New Client`
- `New Gerbil exits`
- `User plays New Client`
- `New Client exits`

Next time they run the launcher (which has been overwritten with the New Gerbil), they get normal flow again (with New Manifest, New Client)

### Not done yet
* Error checking is not completely robust, and there is no retry mechanism
* max-version not checked
* updating the launcher is doable but tricky
* atomically updating the app is tricky (what if someone is halfway through downloading the current version? the new manifest should point at all-new files, then the manifest itself can be atomically replaced, and existing in-progress downloads won't be affected)
* incremental update by downloading the existing file and manifest from the url specified and checking against the new files, then create a manifest which references existing and new files depending on differences
* a way to download alternate versions for 32 or 64 bit windows
